export const HOST = "http://localhost:5000/api/";


export const setHeader = (token) => {

    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    if(token){
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        } 
    }

    let config = {
        headers: headers
    };
    return config;
}