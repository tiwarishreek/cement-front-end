import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";
// import { AdminRoute } from "./AdminRoute";

import Home from "./components/Home";
import About from "./components/About";
import Dealer from "./components/Dealer";
import Customer from "./components/Customer";
import Login from "./components/Login";
import Profile from "./components/Profile";
import Contact from "./components/Contact";

import StateList from "./components/StateList";
import StateForm from "./components/StateForm";

import CityList from "./components/CityList";
import CityForm from "./components/CityForm";

import LocationList from "./components/LocationList";
import LocationForm from "./components/LocationForm";

import MaterialList from "./components/MaterialList";
import MaterialForm from "./components/MaterialForm";

import TypeList from "./components/TypeList";
import TypeForm from "./components/TypeForm";

import BrandList from "./components/BrandList";
import BrandForm from "./components/BrandForm";

import UserList from "./components/UserList";

import ProductList from "./components/ProductList";
import ProductForm from "./components/ProductForm";
import ProductEditForm from "./components/ProductEditForm";

import ViewProduct from "./components/ViewProduct";

import Terms from "./components/Terms";


function Routes() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/view-product" component={ViewProduct} />
        {/* <Route path="/home" component={Home} /> */}
        <Route path="/about" component={About} />
        <PrivateRoute path="/home" component={Home} />
        <Route path="/dealer" component={Dealer} />

        <Route path="/profile" component={Profile} />

        <Route path="/profile" component={Profile} />
        <PrivateRoute path="/dealer-profile" component={Profile} />
        <PrivateRoute path="/customer-profile" component={Profile} />

        <Route path="/customer" component={Customer} />
        <PublicRoute path="/login" component={Login} />
        <Route path="/contact" component={Contact} />

        {/* State Routes */}
        <Route path="/states" component={StateList} />
        <Route path="/state-form" component={StateForm} />

        {/* City Routes */}
        <Route path="/cities" component={CityList} />
        <Route path="/city-form" component={CityForm} />

        {/* Location Routes */}
        <Route path="/locations" component={LocationList} />
        <Route path="/location-form" component={LocationForm} />

        {/* Material Routes */}
        <Route path="/materials" component={MaterialList} />
        <Route path="/material-form" component={MaterialForm} />

        {/* Types Routes */}
        <Route path="/types" component={TypeList} />
        <Route path="/type-form" component={TypeForm} />

        {/* Brand Routes */}
        <Route path="/brands" component={BrandList} />
        <Route path="/brand-form" component={BrandForm} />

        {/* Users Routes */}
        <Route path="/users" component={UserList} />

        {/* Brand Routes */}
        <Route exact path="/products" component={ProductList} />
        <Route exact path="/product-form" component={ProductForm} exact />
        <Route exact path="/product-form/edit/:id" component={ProductEditForm} />

        <Route exact path="/terms" component={Terms} />
      </Switch>
    </Router>
  );
}

export default Routes;
