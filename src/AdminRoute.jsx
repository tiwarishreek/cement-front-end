import React, { Component } from 'react';
import auth from "./services/auth";

import { BrowserRouter as Router, Route, Switch,Redirect } from 'react-router-dom'

export const AdminRoute = ({ component: Component, ...rest }) => (
    
    <Route {...rest} render={(props) => (

      // console.log(auth.getAuth());
      (auth.getAuth().isAuthenticated === "true" && auth.getAuth().user.user.role === "Admin") 
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )

