import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';

import Layout from "./Layouts/Layout";
import { searchProduct } from "./../services/product";

import AuthService from "./../services/auth";
import CartService from "./../services/cart";

class ViewProduct extends Component {

  state = {
    data: Object,
    qty: 0,
    amount: 0,
    user: {
      company_name: ''
    },
    form: {
      delivery_address: '',
      site_in_charge_name: '',
      contact_no_site_person: ''
    },
    error: false,
    errors: {
      delivery_address: '',
      site_in_charge_name: '',
      contact_no_site_person: ''
    }
  }
  constructor(props) {
    super(props);
    console.log("id : >> " + JSON.stringify(props.match.params.id));
  }

  componentDidMount() {


    this.getProductDetails();
  }

  handlePayClick = () => {
    if (!AuthService.checkAuth()) {
      this.props.history.push("/login");
      return;
    }
    if (AuthService.getRole() !== "Customer") {
      AuthService.logout();
      this.props.history.push("/login");
      return;
    }
  }

  changeQty = (evt) => {

    let { data, qty, amount } = this.state;
    let required_qty = evt.target.value;
    let min_qty = data?.min_qty ?? 1;
    let stock = data?.stock ?? 10000;


    if (required_qty >= min_qty && required_qty <= stock) {
      qty = required_qty;
    }
    amount = qty * data.price;
    CartService.setCart({
      id: data._id,
      qty
    });
    this.setState({
      ...this.state,
      qty,
      amount
    });
  }

  getProductDetails = async () => {
    // let { id } = this.props.match.params;

    let cart = await CartService.getCart();
    if (cart === null) {
      this.props.history.push("/");
      return;
    }

    let id = cart.id;

    searchProduct({ id, customer: true }).then((response) => {
      let data = response.data.result[0];
      let qty = cart.qty;
      let amount = qty * data.price;
      this.setState({ ...this.state, data, qty, amount });
    }).catch((error) => {
      console.log(`\n>> Error >\n ${JSON.stringify(error)}`);
    });
  }

  handleChange = (evt) => {
    let { error,errors, form } = this.state;
    const value = evt.target.value;
    if (!value) {
      error = true;
      errors = { ...errors, [evt.target.name]: `${evt.target.name} is required` };
    } else {
      error = false;
      errors = { ...errors, [evt.target.name]: '' };
    }
    form = { ...form, [evt.target.name]: value };
    this.setState({
      ...this.state,
      error,
      errors,
      form
    });
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    let { error, errors, form } = this.state;
    for (let key in form) {
      if (form.hasOwnProperty(key)) {
        let val = form[key];
        if (val === '' || val.length === 0) {
          error = true;
          errors[key] = `${key.replace('_', ' ')} field is required`;
        } else {
          errors[key] = '';
        }
      }
    }

    this.setState((prevState) => ({
      errors: errors,
      error: error,
    }));

  
  }


  getTable = () => {
    let { data, qty, amount } = this.state;
    return (
      <tr key={data._id}>
        <td>1</td>
        <td>
          {data?.brand?.type?.name ?? ''} {data?.brand?.type?.material?.name ?? ''}
        </td>
        <td>
          {data?.brand?.name ?? '-'}
        </td>
        <td>
          <input className="form-control" type="number" name="qty" value={qty} onChange={this.changeQty} placeholder="Qty" />
        </td>
        <td>
          <input readonly className="form-control" type="number" name="price" value={data.price} placeholder="Price per bag" />
        </td>
        <td>
          <input className="form-control" type="number" name="amount" value={amount} placeholder="Amount" />
        </td>
        {/* <td>
          <button className="btn btn-success" onClick={() => this.handlePayClick()}> Pay </button>
        </td> */}
      </tr>
    )

  }
  render() {

    let { form,errors } = this.state;
    let user = AuthService.getAuthUser();

    return (
      <React.Fragment>
        <Layout title="View Cart" >
          <div className="page-content">
            <div className="container">

              <div className="row">
                <div className="col-md-12">
                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          Cart Item
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body">
                      <div className="table-scrollable">
                        <table className="table table-striped table-bordered table-advance table-hover">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Type</th>
                              <th>Brand</th>
                              <th>Order Qty</th>
                              <th>Rate (Rs/Bag) </th>
                              <th>Amount</th>
                            </tr>
                          </thead>
                          <tbody>{this.getTable()}</tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          Customer Details
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body form">

                      <div className="card-body">

                        <div className="row">
                          <div className="col-md-4">
                            <label> Name :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="company_name"
                              value={user.name}
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>

                        <div className="row">
                          <div className="col-md-4">
                            <label>Company Name :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="company_name"
                              value={user['profile'][0]?.company }
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-4">
                            <label>Contact No :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="company_name"
                              value={user['mobile']}
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-4">
                            <label>Office Address :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="address"
                              value={user['address']}
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-4">
                            <label>Email :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="company_name"
                              value={user['email']}
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-4">
                            <label>GST No :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="company_name"
                              value={user['profile'][0].gst_no}
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-4">
                            <label>Pan No :</label>
                          </div>
                          <div className="form-group col-md-8">
                            <input
                              name="company_name"
                              value={user['profile'][0].pan_no}
                              type="text"
                              placeholder="Company Name"
                              className="form-control form-control-sm"
                              readOnly
                            />
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          Delivery Address Details
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body">
                      <div className="card-body">
                        <form onSubmit={this.handleSubmit}>
                          <div className="row">
                            <div className="col-md-4">
                              <label>Site In-Charge Name  :</label>
                            </div>
                            <div className="form-group col-md-8">
                              <input
                                name="site_in_charge_name"
                                value={form['site_in_charge_name']}
                                type="text"
                                placeholder="Site In-Charge Name"
                                className="form-control form-control-sm"
                                onChange={this.handleChange}

                              />

                              {
                                errors.site_in_charge_name ? <div className="text-danger">
                                  {errors.site_in_charge_name}
                                </div> : ''
                              }
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-4">
                              <label>Contact No - Site Person   :</label>
                            </div>
                            <div className="form-group col-md-8">
                              <input
                                name="contact_no_site_person"
                                value={form['contact_no_site_person']}
                                type="text"
                                placeholder="Contact No - Site Person"
                                className="form-control form-control-sm"
                                onChange={this.handleChange}
                              />
                              {
                                errors.contact_no_site_person ? <div className="text-danger">
                                  {errors.contact_no_site_person}
                                </div> : ''
                              }
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-4">
                              <label>Delivery Address  :</label>
                            </div>
                            <div className="form-group col-md-8">
                              <textarea name="delivery_address"
                                type="text"
                                placeholder="Delivery Address"
                                onChange={this.handleChange}
                                className="form-control form-control-sm">{form['delivery_address']}</textarea>
                              {
                                errors.delivery_address ? <div className="text-danger">
                                  {errors.delivery_address}
                                </div> : ''
                              }
                            </div>
                          </div>

                          <div className="row">

                              <div className="col-md-12">
                                <h5>
                                  Terms & Condition:
                                </h5>
                              <label>
                                <input type="checkbox" name="agreement" checked/> 
                              </label>
                              <p>
                            Your <strong> { user.name.toUpperCase() } </strong> access to and use of Lorem Ipsum (the app) is subject exclusively to these Terms and Conditions. You will not use the app for any purpose that is unlawful or prohibited by these Terms and Conditions. By using the app you are fully accepting the terms, conditions and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the app.
                              </p>
                              </div>
                          </div>

                          <button type="submit" className="btn btn-success pull-right" style={{ marginLeft: "10px" }}>Payment</button>

                          <button type="submit" className="btn btn-info pull-right">Back</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

export default withRouter(ViewProduct)
