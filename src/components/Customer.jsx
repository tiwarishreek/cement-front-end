import React, { Component } from 'react'
import Layout from "./Layouts/Layout";
import { getState } from "../services/state";
import { getCity as getCityService } from "../services/city";
import { getLocation as getLocationService } from "../services/location";

import {
    postUser as postUserService,
    validateEmail as validateEmailService
} from "../services/user";

import { Link } from 'react-router-dom';

class Customer extends Component {

    state = {
        states: [],
        cities: [],
        locations: [],


        message: '',
        type: 'danger',

        error: false,

        errors: {
            name: '',
            email: '',
            mobile: '',
            password: '',
            city: '',
            location: '',
            address: '',
            pin_code: '',
            role: 'Customer',

            company_name: '',
            phone_no: '',
            pan_no: '',
            gst_no: '',

            state: '',

            confirm_password: '',
        },


        form: {
            name: '',
            email: '',
            mobile: '',
            password: '',
            city: '',
            location: '',
            address: '',
            pin_code: '',
            role: 'Customer',

            company_name: '',
            phone_no: '',
            pan_no: '',
            gst_no: '',

            state: '',

            confirm_password: '',
            // agreement: ''
        },
        registration_error: '',
        user: ''
    }

    componentDidMount() {
        getState().then((response) => {
            this.setState({ ...this.state, states: response.data.result });
        }).catch((error) => {
            console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
        });

    }

    getCity = (event) => {
        const stateId = event.target.value;
        getCityService(stateId).then((response) => {
            let { cities, form } = this.state;
            form = { ...form, state: stateId };
            cities = response.data.result;
            this.setState({ ...this.state, cities: cities, form: form });
        }).catch((error) => {
            console.log(`\n>>City Error >\n ${JSON.stringify(error)}`);
        })
    }

    getLocation = (event) => {
        const cityId = event.target.value;
        getLocationService(cityId).then((response) => {
            let { locations, form } = this.state;
            form = { ...form, city: cityId };
            locations = response.data.result;
            this.setState({ ...this.state, locations: locations, form: form });
        }).catch((error) => {
            console.log(`\n>>Location Error >\n ${JSON.stringify(error)}`);
        })
    }


    handleChange = (event) => {
        var form = { ...this.state.form }

        let input = event.target.name;

        form[event.target.name] = event.target.value;

        // this.setState({ ...this.state, form })
        this.setState({ ...this.state, form });
        // console.log("\n>>form >> \n", JSON.stringify(this.state.form));
    }

    handleSubmit = async (event) => {
        event.preventDefault();

        // extract state fields
        let { error, errors, form } = this.state;
        error = false;
        // General Validation
        for (let key in form) {
            if (form.hasOwnProperty(key)) {
                let val = form[key];
                if (val === '' || val.length === 0) {
                    error = true;
                    errors[key] = `${key.replace('_', ' ')} field is required`;
                    console.log(` key : ${key}, val : ${val}`);

                } else {
                    errors[key] = '';
                }
            }
        }
        // email validation
        let email_regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        // let email_regex = /^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/;
        const validateEmail = email_regex.test(form.email);
        if (validateEmail === false) {
            error = true;
            errors.email = "Please enter valid email";
        } else {

            try {
                // response.data.result
                let validateEmailErrorFlag = await validateEmailService(form.email);
                // alert(JSON.stringify(validateEmailErrorFlag.data.result));
                if (validateEmailErrorFlag.data.result === true) {
                    error = true;
                    errors.email = "Email is already used.";
                } else {
                    errors.email = "";
                }
            } catch (err) {

            }
        }

        this.setState((prevState) => ({
            errors: errors,
            error: error,
        }));
        // alert(error);
        if (error === false) {

            // call post user service

            const { form } = this.state;
            postUserService(form).then((response) => {
                console.log(`\n>>User Response >\n ${JSON.stringify(response)}`);
                this.setState({
                    ...this.state,
                    message: "registration is successful",
                    type: 'success'
                });
                setTimeout(() => {
                    this.props.history.push('/login');
                }, 2000)
            }).catch((error) => {
                this.setState({
                    ...this.state,
                    // registration_error: error.response.data.message,
                    message: error.response.data.message,
                    type: 'danger'
                });
                console.log(`\n>> Error >\n ${JSON.stringify(error.response)}`);
            })
        }

    }



    render() {

        const {
            states,
            cities,
            locations,
            form,
            errors
        } = this.state;

        const StateList = states.length > 0 ? states.map((state) => {
            return <option value={state._id} key={state._id}>{state.name}</option>
        }) : '';

        const cityList = cities.length > 0 ? cities.map((city) => {
            return <option value={city._id} key={city._id}>{city.name}</option>
        }) : '';
        const locationList = locations.length > 0 ? locations.map((location) => {
            return <option value={location._id} key={location._id}>{location.name}</option>
        }) : '';




        let alertClass = `alert alert-${this.state.type} alert-dismissible`;


        return (
            <React.Fragment>
                <Layout title="Customer Registration" >
                    <div className="page-content">
                        <div className="container">

                            <div className="row">
                                <div className="col-md-12">
                                    {
                                        this.state.message !== '' ? <div className={alertClass} >
                                            {this.state.message}
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div> : ''
                                    }
                                    <div className="portlet light">
                                        <div className="portlet-title">
                                            <div className="caption">
                                                <i className="fa fa-cogs font-green-sharp"></i>
                                                <span className="caption-subject font-green-sharp bold uppercase">
                                                    Customer Registration Form
                                                </span>
                                            </div>
                                        </div>
                                        <div className="portlet-body form">
                                            <form onSubmit={this.handleSubmit}>
                                                {/* <div className="card-header">Dealer Registration</div> */}
                                                <div className="card-body">

                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>Company Name</label>
                                                        </div>
                                                        <div className="form-group col-md-8">
                                                            <input
                                                                name="company_name"
                                                                value={form['company_name']}
                                                                onChange={this.handleChange}

                                                                type="text"
                                                                placeholder="Company Name"
                                                                className="form-control form-control-sm"
                                                            />

                                                            {
                                                                errors.company_name ? <div className="text-danger">
                                                                    {errors.company_name}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>Name</label>
                                                        </div>
                                                        <div className="form-group col-md-8">
                                                            <input
                                                                name="name"
                                                                value={form['name']}
                                                                onChange={this.handleChange}
                                                                placeholder="Name"
                                                                type="text"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.name ? <div className="text-danger">
                                                                    {errors.name}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>Contact No / Phone No </label>
                                                        </div>
                                                        <div className="form-group col-md-8">
                                                            <input
                                                                name="phone_no"
                                                                value={form['phone_no']}
                                                                onChange={this.handleChange}
                                                                placeholder="Contact No / Phone No"
                                                                type="text"
                                                                maxLength="10"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.phone_no ? <div className="text-danger">
                                                                    {errors.phone_no}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>Mobile No </label>
                                                        </div>
                                                        <div className="form-group col-md-8">
                                                            <input
                                                                name="mobile"
                                                                value={form['mobile']}
                                                                onChange={this.handleChange}
                                                                placeholder="Mobile No "
                                                                type="text"
                                                                maxLength="10"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.mobile ? <div className="text-danger">
                                                                    {errors.mobile}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>Office Address</label>
                                                        </div>
                                                        <div className="form-group col-md-8">
                                                            <textarea
                                                                name="address"
                                                                value={form['address']}
                                                                onChange={this.handleChange}
                                                                placeholder="Office Address"
                                                                className="form-control form-control-sm"
                                                                name="address"
                                                            ></textarea>
                                                            {
                                                                errors.address ? <div className="text-danger">
                                                                    {errors.address}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>Email ID</label>
                                                        </div>
                                                        <div className="form-group col-md-8">
                                                            <input
                                                                name="email"
                                                                value={form['email']}
                                                                onChange={this.handleChange}
                                                                placeholder="Email ID"
                                                                type="text"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.email ? <div className="text-danger">
                                                                    {errors.email}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Pan No</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <input
                                                            name="pan_no"
                                                            value={form['pan_no']}
                                                            onChange={this.handleChange}
                                                            placeholder="Pan No"
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.pan_no ? <div className="text-danger">
                                                                {errors.pan_no}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>GST No</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <input
                                                            name="gst_no"
                                                            value={form['gst_no']}
                                                            onChange={this.handleChange}
                                                            placeholder="GST No"
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.gst_no ? <div className="text-danger">
                                                                {errors.gst_no}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Select Your State</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <select
                                                            name="state"
                                                            className="form-control form-control-sm"
                                                            onChange={this.getCity}
                                                        >
                                                            <option value="">Select Option</option>
                                                            {StateList}
                                                        </select>
                                                        {
                                                            errors.state ? <div className="text-danger">
                                                                {errors.state}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Select District / City</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <select
                                                            name="city"
                                                            onChange={this.getLocation}
                                                            className="form-control form-control-sm"
                                                        >
                                                            <option value="">Select Option</option>
                                                            {cityList}
                                                        </select>
                                                        {
                                                            errors.city ? <div className="text-danger">
                                                                {errors.city}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Select Location</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <select
                                                            name="location"
                                                            onChange={this.handleChange}
                                                            className="form-control form-control-sm"
                                                        >
                                                            <option value="">Select Option</option>
                                                            {locationList}
                                                        </select>
                                                        {
                                                            errors.location ? <div className="text-danger">
                                                                {errors.location}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Pin Code</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <input
                                                            name="pin_code"
                                                            value={form['pin_code']}
                                                            onChange={this.handleChange}
                                                            placeholder="Pin Code - 6 digit"
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.pin_code ? <div className="text-danger">
                                                                {errors.pin_code}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>



                                                <h4 className="card-title">
                                                    <b>Create Passwords</b>
                                                </h4>
                                                <hr />
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Email ID</label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <input
                                                            name="email"
                                                            value={form['email']}
                                                            onChange={this.handleChange}
                                                            placeholder="Email ID "
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                            readOnly
                                                        />
                                                        {
                                                            errors.email ? <div className="text-danger">
                                                                {errors.email}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Create Password</label>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        <input
                                                            name="password"
                                                            value={form['password']}
                                                            onChange={this.handleChange}
                                                            placeholder="Create Password"
                                                            type="password"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.password ? <div className="text-danger">
                                                                {errors.password}
                                                            </div> : ''
                                                        }

                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>Confirm Password </label>
                                                    </div>
                                                    <div className="form-group col-md-8">
                                                        <input
                                                            name="confirm_password"
                                                            value={form['confirm_password']}
                                                            onChange={this.handleChange}
                                                            placeholder="Confirm Password"
                                                            type="password"
                                                            maxLength="10"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.confirm_password ? <div className="text-danger">
                                                                {errors.confirm_password}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="form-group form-check">
                                                            <input
                                                                // name="agreement"
                                                                value={form['agreement']}
                                                                // onChange={this.handleChange}
                                                                type="checkbox"
                                                                className="form-check-input"
                                                                checked
                                                            />
                                                            
                                                            <label className="form-check-label">
                                                                <Link to="/terms" target="_blank">
                                                                    I agree, to all terms & condition
                                                                </Link>
                                                            </label>
                                                        </div>
                                                        {
                                                            errors.agreement ? <div className="text-danger">
                                                                {errors.agreement}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>

                                                {/* Form Code is Closed */}

                                                <div className="row">
                                                    <div className="col-md-12">
                                                        {
                                                            this.state.message !== '' ? <div className="alert alert-warning alert-dismissible" >
                                                                {this.state.message}
                                                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="form-group col-md-4">
                                                    <button type="submit" className="btn btn-primary">Submit</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}

export default Customer
