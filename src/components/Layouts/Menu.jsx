import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import auth from "./../../services/auth";

class Menu extends Component {
    
    handleLogout = (event) => {
        event.preventDefault();
        auth.logout();
        this.props.history.push('/login');
    }

    render() {
        return (

            <div className="page-header">
                {/* <!-- BEGIN HEADER TOP --> */}
                <div className="page-header-top">
                    <div className="container">
                        {/* <!-- BEGIN LOGO --> */}
                        <div className="page-logo">
                            <Link to="/">
                            {/* <img src="../../assets/admin/layout3/img/logo-default.png" alt="logo" className="logo-default" /> */}
                            <img src="hippo.jpg" alt="logo" className="logo-default" />
                            </Link>
                        </div>
                        {/* <!-- END LOGO --> */}
                        {/* <!-- BEGIN RESPONSIVE MENU TOGGLER --> */}
                        <a className="menu-toggler"></a>
                        {/* <!-- END RESPONSIVE MENU TOGGLER --> */}
                        {/* <!-- BEGIN TOP NAVIGATION MENU --> */}
                        <div className="top-menu">
                            <ul className="nav navbar-nav pull-right">


                                <li className="dropdown dropdown-user dropdown-dark">

                                    <Link to="/">
                                        Contact Us - +91-9867976493
                                    </Link>
                                </li>
                                <li className="dropdown dropdown-user dropdown-dark">
                                    <Link to="/">
                                        Become distributor
                                    </Link>
                                </li>

                                {
                                    auth.getAuth().isAuthenticated == "true" ?
                                        <React.Fragment>
                                            <li className="droddown dropdown-separator">
                                                <span className="separator"></span>
                                            </li>
                                            <li className="dropdown dropdown-user dropdown-dark">
                                                <a className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                    <img alt="" className="img-circle" src="../../assets/admin/layout3/img/avatar9.jpg" />
                                                    <span className="username username-hide-mobile">{
                                                        auth.getAuth().user.user.name
                                                    }</span>
                                                </a>
                                                <ul className="dropdown-menu dropdown-menu-default">
                                                    <li>
                                                        <Link to="/profile">
                                                            <i className="icon-user"></i>
                                                            My Profile
                                                        </Link>
                                                    </li>
                                                    <li className="divider"></li>
                                                    <li>
                                                        <a onClick={this.handleLogout}>
                                                            <i className="icon-key"></i>
                                                            Log Out
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li> </React.Fragment> : ''
                                }


                            </ul>
                        </div>
                    </div>
                </div>
                {/* <!-- END HEADER TOP --> */}
                {/* <!-- BEGIN HEADER MENU --> */}
                <div className="page-header-menu">
                    <div className="container">
                        <div className="hor-menu ">
                            <ul className="nav navbar-nav">
                                <li className="active">
                                    <Link className="nav-link" to="/">
                                        Home
                                    </Link>
                                </li>


                                {
                                    (auth.getAuth().isAuthenticated == "true" && auth.getAuth().user.user.role === 'Dealer') ? <React.Fragment>
                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Dealer
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/dealer-profile">
                                                        <i className="icon-user"></i>
                                                        Profile
                                                    </Link>
                                                </li>
                                                
                                            </ul>
                                        </li>

                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Products
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/products">
                                                        <i className="icon-user"></i>
                                                        View Products
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link className="nav-link" to="/product-form">
                                                        <i className="icon-user"></i>
                                                        Add Products
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li>
                                    </React.Fragment> : ''
                                }

                                {
                                    (auth.getAuth().isAuthenticated == "true" && (auth.getAuth().user.user.role === 'Customer' || auth.getAuth().user.user.role === '')) ? <React.Fragment>
                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                User
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/dealer-profile">
                                                        <i className="icon-user"></i>
                                                        Profile
                                                    </Link>
                                                </li>
                                                <li className="">
                                                    <Link className="nav-link" to="/profile">
                                                        Profile
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link className="nav-link" to="/dealer-profile">
                                                        <i className="icon-user"></i>
                                                        Orders
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li>
                                    </React.Fragment> : ''
                                }

                                {
                                    (auth.getAuth().isAuthenticated === "true" && auth.getAuth().user.user.role === 'Admin') ? <React.Fragment>
                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Masters
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">

                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        State 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/states">
                                                                <i className="icon-user"></i>
                                                                View State
                                                            </Link>
                                                        </li>
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/state-form">
                                                                <i className="icon-user"></i>
                                                               Add State
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        City 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/cities">
                                                                <i className="icon-user"></i>
                                                                View City
                                                            </Link>
                                                        </li>
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/city-form">
                                                                <i className="icon-user"></i>
                                                               Add City
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                                
                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        Location 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/locations">
                                                                <i className="icon-user"></i>
                                                                View Location
                                                            </Link>
                                                        </li>
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/location-form">
                                                                <i className="icon-user"></i>
                                                               Add Location
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        Material 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/materials">
                                                                <i className="icon-user"></i>
                                                                View Material
                                                            </Link>
                                                        </li>
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/material-form">
                                                                <i className="icon-user"></i>
                                                               Add Material
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        Type 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/types">
                                                                <i className="icon-user"></i>
                                                                View Type
                                                            </Link>
                                                        </li>
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/type-form">
                                                                <i className="icon-user"></i>
                                                               Add Type
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        Brands 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/brands">
                                                                <i className="icon-user"></i>
                                                                View Brands
                                                            </Link>
                                                        </li>
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/brand-form">
                                                                <i className="icon-user"></i>
                                                               Add Brands
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className=" dropdown-submenu">
                                                    <a>
                                                        <i className="icon-briefcase"></i>
                                                        Users 
                                                    </a>
                                                    <ul className="dropdown-menu">
                                                        <li className=" ">
                                                            <Link className="nav-link" to="/users">
                                                                <i className="icon-user"></i>
                                                                View Users
                                                            </Link>
                                                        </li>
                                                        
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Dealers
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/dealer-profile">
                                                        <i className="icon-user"></i>
                                                        Products
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li>

                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Products
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/products">
                                                        <i className="icon-user"></i>
                                                        View Products
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link className="nav-link" to="/product-form">
                                                        <i className="icon-user"></i>
                                                        Add Products
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li>
                                    </React.Fragment> : ''
                                }




                                {
                                    auth.getAuth().isAuthenticated !== "true" ? <React.Fragment>

                                        
                                        <li className="">
                                            <Link className="nav-link" to="/about">
                                                About Us
                                        </Link>
                                        </li>

                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Dealer
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/dealer">
                                                        <i className="icon-user"></i>
                                                        Registration
                                                </Link>
                                                </li>
                                                <li>
                                                    <Link className="nav-link" to="/login">
                                                        <i className="icon-user"></i>
                                                        Login
                                                </Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"  >
                                                Customer
                                            <i className="fa fa-angle-down"></i>
                                            </a>
                                            <ul className="dropdown-menu pull-left">
                                                <li>
                                                    <Link className="nav-link" to="/customer">
                                                        <i className="icon-user"></i>
                                                        Registration
                                                </Link>
                                                </li>
                                                <li>
                                                    <Link className="nav-link" to="/login">
                                                        <i className="icon-user"></i>
                                                        Login
                                                </Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="">
                                            <Link className="nav-link" to="/contact">
                                                Contact Us
                                        </Link>
                                        </li>
                                        <li className="">
                                            <Link className="nav-link" to="/terms">
                                                Terms & Conditions
                                            </Link>
                                        </li>
                                        {/* <li className="">
                                        <Link className="nav-link" to="/login">
                                            Login
                                        </Link>
                                    </li> */}
                                    </React.Fragment> : ''
                                }


                            </ul>
                        </div>
                        {/* <!-- END MEGA MENU --> */}
                    </div>
                </div>
                {/* <!-- END HEADER MENU --> */}
            </div>
        )
    }
}

export default withRouter(Menu)
