import React, { Component } from 'react'
import Menu from "./Menu";


export default class Layout extends Component {

  render() {
    return (
      <React.Fragment>
        <div>
          <Menu></Menu>
          <div className="page-container">
            {/* <!-- BEGIN PAGE HEAD --> */}
            <div className="page-head">
              <div className="container">
                {/* <!-- BEGIN PAGE TITLE --> */}
                <div className="page-title">
                  <h1>
                    {this.props.title || '' } 
                    <small>
                      { this.props.small_title || ' '}
                    </small>
                  </h1>
                </div>
                {/* <!-- END PAGE TITLE --> */}
              </div>
            </div>

            {/* Children Component */}

            {this.props.children}
          </div>

          <div className="page-footer">
            <div className="container">
              { new Date().getFullYear() } &copy; Metronic by keenthemes.
            </div>
          </div>
          <div className="scroll-to-top">
            <i className="icon-arrow-up"></i>
          </div>
        </div>
      </React.Fragment>
    );
  }
}