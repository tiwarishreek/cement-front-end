import React, { Component } from 'react'
import Menu from "./Layouts/Menu";
import auth from "../services/auth";
import { login as loginService } from "../services/user";

import {Link} from "react-router-dom";
class Login extends Component {

    state = {
        message: '',
        error: false,

        errors: {
            email: '',
            password: ''
        },

        login: {
            email: '',
            password: ''
        },

        processing: false
    }



    handleChange = (evt) => {
        let { error, login } = this.state;
        const value = evt.target.value;
        if (!value) {
            error = true;
        }
        login = { ...login, [evt.target.name]: value }
        this.setState({
            ...this.state,
            error,
            login
        });
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        let email_regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        let { error, errors, login } = this.state;

        const validateEmail = email_regex.test(login.email);
        if (validateEmail == false) {
            error = true;
            errors.email = "Please enter valid email";
        } else {
            error = false;
            errors.email = "";
        }
        if (login.password == '') {
            error = true;
            errors.password = "Please enter password";
        } else {
            error = false;
            errors.password = "";
        }

        this.setState((prevState) => ({
            errors: errors,
            error: error,
        }));
        if (error == false) {
            loginService(this.state.login).then((response) => {
                // console.log(JSON.stringify(response));
                let result = response.data; 
                let redirect_to = "/home";
                if(result.user.role === "Customer" ){
                    redirect_to = "/view-product";
                }
                auth.authenticate(response.data);
                // console.log(JSON.stringify(auth.getAuth()));
                this.props.history.push(redirect_to);
            }).catch((error) => {
                this.setState({
                    ...this.state,
                    message: error.response.data.message
                })
            })
        }
    }



    render() {

        const { error, errors, login } = this.state;

        return (

            <div>
                <Menu></Menu>
                <div className="page-container">
                    {/* <!-- BEGIN PAGE HEAD --> */}
                    <div className="page-head">
                        <div className="container">
                            {/* <!-- BEGIN PAGE TITLE --> */}
                            <div className="page-title">
                                <h1>Login <small>login page</small></h1>
                            </div>
                            {/* <!-- END PAGE TITLE --> */}
                        </div>
                    </div>
                    {/* <!-- END PAGE HEAD --> */}
                    {/* <!-- BEGIN PAGE CONTENT --> */}
                    <div className="page-content">
                        <div className="container">
                            {/* <!-- BEGIN PAGE BREADCRUMB --> */}
                            <ul className="page-breadcrumb breadcrumb">
                                <li className="">
                                    <a href="#">Home</a><i className="fa fa-circle"></i>
                                </li>
                                <li className="active">
                                    Login
				                </li>
                            </ul>
                            {/* <!-- END PAGE BREADCRUMB --> */}

                            <div className="row">
                                <div className="col-md-12">
                                    {
                                        this.state.message ? <div className="alert alert-danger alert-dismissible" >
                                            {this.state.message}
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div> : ''
                                    }

                                    <div className="portlet light">
                                        <div className="portlet-title">
                                            <div className="caption">
                                                <i className="fa fa-cogs font-green-sharp"></i>
                                                <span className="caption-subject font-green-sharp bold uppercase">
                                                    Login Form
                                                </span>
                                            </div>
                                        </div>
                                        <div className="portlet-body form">
                                            <form onSubmit={this.handleSubmit}>
                                                <div className="form-group">
                                                    <label>Email address</label>
                                                    <input
                                                        name="email"
                                                        value={login.email}
                                                        onChange={this.handleChange}
                                                        type="email" 
                                                        className="form-control" 
                                                        placeholder="Enter email" />

                                                        {
                                                            errors.email ? <div className="text-danger">{errors.email}</div> : ''
                                                        }

                                                    <small className="form-text text-muted">We'll never share your email with anyone else.</small>
                                                </div>
                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <input
                                                        name="password"
                                                        value={login.password}
                                                        onChange={this.handleChange}

                                                        type="password" className="form-control" placeholder="Password" />
                                                    {
                                                        errors.password ? <div className="text-danger">{errors.password}</div> : ''
                                                    }
                                                </div>

                                                
                                                {/* <div className="form-group margin-top-20 margin-bottom-20">
                                                    <label className="check">
                                                    <div className="checker">
                                                        <span>
                                                            <input type="checkbox" name="tnc" />
                                                        </span>
                                                    </div> I agree to the  
                                                    <a href="javascript:;">
                                                    Terms of Service </a>
                                                    &amp; <a href="javascript:;">
                                                    Privacy Policy </a>
                                                    </label>
                                                    <div id="register_tnc_error">
                                                    </div>
                                                </div> */}

                                                <button type="submit" className="btn btn-primary">Submit</button>
                                            </form>

                                            <div className="form-group margin-top-20 margin-bottom-20">
                                                <Link to="/customer">
                                                    Create Customer Account 
                                                </Link>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="page-footer">
                    <div className="container">
                        2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                    </div>
                </div>
                <div className="scroll-to-top">
                    <i className="icon-arrow-up"></i>
                </div>
            </div>

        )
    }
}

export default Login
