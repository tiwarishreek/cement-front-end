import React, { Component } from 'react'
import Layout from "./Layouts/Layout";

import sweetAlert from 'sweetalert2';

import { getState } from "../services/state";
import { getCity as getCityService, getAllCity } from "../services/city";
import { updateUserById as updateUserService, getUserById } from "../services/user";

import { getLocation as getLocationService,getAllLocation } from "../services/location";

import auth from "../services/auth";

export class Profile extends Component {

    state = {
        states: [],
        cities: [],
        locations: [],
        type: 'danger',
        errors: {
            name: '',
            email: '',
            mobile: '',
            // password: '',
            city: '',
            location: '',
            address: '',
            // pin_code: '',
            // role: 'Dealer',

            company_name: '',
            phone_no: '',
            pan_no: '',
            gst_no: '',
            // material: [],
            // type: [],
            state: '',
            account_name: '',
            account_no: '',
            bank: '',
            ifsc: '',
            // confirm_password: '',
            // agreement: ''
        },

        form: {
            name: '',
            email: '',
            mobile: '',
            // password: '',
            
            address: '',
            // pin_code: '',
            role: 'Dealer',

            company_name: '',
            phone_no: '',
            pan_no: '',
            gst_no: '',
            // material: [],
            // type: [],
            account_name: '',
            account_no: '',
            bank: '',
            ifsc: '',

            state: '',
            city: '',
            location: '',
            // confirm_password: '',
            // agreement: ''
        },
        message:'',
        registration_error: '',
        user: ''
    }

    constructor(props) {
        super(props);
        let id =  auth.getAuth().user.user._id;
        getUserById(id).then((response) => {
            let result = response.data.result[0];
            let { form } = this.state;
            form = { ...form,
                name:result.name,
                email:result.email,
                mobile:result.mobile,
                address:result.address,
                company_name:result?.profile[0]?.company,
                phone_no:result.phone_no,
                pan_no:result?.profile[0]?.pan_no,
                gst_no:result?.profile[0]?.gst_no,
                account_name:result?.profile[0]?.account_name,
                account_no:result?.profile[0]?.account_no,
                bank:result?.profile[0]?.bank,
                ifsc:result?.profile[0]?.ifsc,
                role:result.role,
                state:result?.location?.city?.state?._id,
                city:result?.location?.city?._id,
                location:result?.location?._id
            }
            this.setState({ ...this.state, form: form });
        }).catch((error) => {
            console.log(`\n>> Error >\n ${JSON.stringify(error)}`);
        })
    }

    componentDidMount() {
        console.log(`\n>> Data >\n ${JSON.stringify(this.state.user)}`);
        getState().then((response) => {
            this.setState({ ...this.state, states: response.data.result });
        }).catch((error) => {
            console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
        });

        getAllCity().then((response) => {
            this.setState({ ...this.state, cities: response.data.result });
        }).catch((error) => {
            console.log(`\n>>City Error >\n ${JSON.stringify(error)}`);
        });

        getAllLocation().then((response) => {
            let { locations, form } = this.state;
            locations = response.data.result;
            this.setState({ ...this.state, locations: locations, form: form });
        }).catch((error) => {
            console.log(`\n>>Location Error >\n ${JSON.stringify(error)}`);
        })
    }

    getCity = (event) => {
        console.log("city called", event.target.value);
        const stateId = event.target.value;
        getCityService(stateId).then((response) => {
            // console.log(`\n>>City Response >\n ${JSON.stringify(response.data)}`);
            this.setState({ ...this.state, cities: response.data.result });
        }).catch((error) => {
            console.log(`\n>>City Error >\n ${JSON.stringify(error)}`);
        })
    }

    getLocation = (event) => {
        const cityId = event.target.value;
        getLocationService(cityId).then((response) => {
            let { locations, form } = this.state;
            form = { ...form, city: cityId };
            locations = response.data.result;
            this.setState({ ...this.state, locations: locations, form: form });
        }).catch((error) => {
            console.log(`\n>>Location Error >\n ${JSON.stringify(error)}`);
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        



        let { error, errors, form } = this.state;
        error = false;
        // General Validation
        for (let key in form) {
            if (form.hasOwnProperty(key)) {
                let val = form[key];
                if (val === '' || val.length === 0) {
                    error = true;
                    errors[key] = `${key.replace('_', ' ')} field is required`;
                    console.log(` key : ${key}, val : ${val}`);
                } else {
                    errors[key] = '';
                }
            }
        }
        this.setState((prevState) => ({
            errors: errors,
            error: error,
        }));

        if (error === false) {
            let id =  auth.getAuth().user.user._id;
            updateUserService(id,form).then((response) => {
                sweetAlert.fire(
                    'Update Profile',
                    'Profile is updated.',
                    'success'
                );
            }).catch((error) => {
                let msg = error.response.data.message;
                sweetAlert.fire(
                    'Update Profile',
                    msg,
                    'error'
                );

                this.setState({
                    ...this.state,
                    message: msg
                });
            });
        }else{
            sweetAlert.fire(
                'Update Profile',
                "Validation Issue - Please check form",
                'error'
            );
        }
        

    }

    handleChange = (event) => {
        // console.log(`\n>>event>\n ${event.target.name} - ${event.target.value}`);
        var form = { ...this.state.form }
        form[event.target.name] = event.target.value;
        this.setState({ ...this.state, form })
        // this.setState({ ...this.state, form });
        // console.log("\n>>form >> \n", JSON.stringify(this.state.form));
    }

    render() {


        // const { states, cities, locations } = this.state;

        const {
            states, cities, locations,
            form,
            errors,
        } = this.state;

        const state_id = this.state.form.state;
        const city_id = this.state.form.city;
        const location_id = this.state.form.location;

        console.log(`state_id ${state_id}, city_id ${city_id} location_id ${location_id}`);

        const StateList = states.length > 0 ? states.map((state) => {
            return <option value={state._id} key={state._id} selected={state_id == state._id}>{state.name}</option>
        }) : '';

        const cityList = cities.length > 0 ? cities.map((city) => {
            return <option value={city._id} key={city._id} selected={city_id == city._id}>{city.name}</option>
        }) : '';
        const locationList = locations.length > 0 ? locations.map((location) => {
            return <option value={location._id} key={location._id} selected={location_id == location._id}>{location.name}</option>
        }) : '';
        let alertClass = `alert alert-${this.state.type} alert-dismissible`;
        return (

            <React.Fragment>
                <Layout title="Profile Form">
                    <div className="page-content">
                        <div className="container">
                            {/* <!-- BEGIN PAGE BREADCRUMB --> */}
                            <ul className="page-breadcrumb breadcrumb">
                                <li className="">
                                    <a href="#">Home</a><i className="fa fa-circle"></i>
                                </li>
                                <li className="active">
                                    Seller Profile
				                </li>
                            </ul>
                            {/* <!-- END PAGE BREADCRUMB --> */}

                            <div className="row">
                                <div className="col-md-12">
                                    {
                                        this.state.message !== '' ? <div className={alertClass} >
                                            {this.state.message}
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div> : ''
                                    }
                                    <div className="portlet light">
                                        <div className="portlet-title">
                                            <div className="caption">
                                                <i className="fa fa-cogs font-green-sharp"></i>
                                                <span className="caption-subject font-green-sharp bold uppercase">
                                                    Seller Profile
                                                </span>
                                            </div>
                                        </div>
                                        <div className="portlet-body form">
                                            <form onSubmit={this.handleSubmit}>
                                                {/* <div className="card-header">Dealer Registration</div> */}
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h3><b>Company Details</b></h3>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h3><b>Account Details</b></h3>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <label>Company Name</label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            {/* <input type="text" placeholder="Company Name" className="form-control form-control-sm" /> */}

                                                            <input
                                                                name="company_name"
                                                                value={form['company_name']}
                                                                onChange={this.handleChange}
                                                                type="text"
                                                                placeholder="Seller's Company Name"
                                                                className="form-control form-control-sm"
                                                            />

                                                            {
                                                                errors.company_name ? <div className="text-danger">
                                                                    {errors.company_name}
                                                                </div> : ''
                                                            }
                                                        </div>

                                                        <div className="col-md-2">
                                                            <label>Bank A/C</label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            {/* <input placeholder="Bank A/C" type="text" className="form-control form-control-sm" /> */}
                                                            <input
                                                            name="account_no"
                                                            value={form['account_no']}
                                                            onChange={this.handleChange}
                                                            placeholder="Account No"
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.account_no ? <div className="text-danger">
                                                                    {errors.account_no}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <label>Seller Name </label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            {/* <input placeholder="Seller Name" type="text" maxLength="10" className="form-control form-control-sm" /> */}
                                                            <input
                                                                name="name"
                                                                value={form['name']}
                                                                onChange={this.handleChange}
                                                                placeholder="Seller's Name"
                                                                type="text"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.name ? <div className="text-danger">
                                                                    {errors.name}
                                                                </div> : ''
                                                            }
                                                        </div>

                                                        <div className="col-md-2">
                                                            <label>Bank Name </label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            {/* <input placeholder="Bank Name" type="text" maxLength="10" className="form-control form-control-sm" /> */}
                                                            <input
                                                            name="bank"
                                                            value={form['bank']}
                                                            onChange={this.handleChange}
                                                            placeholder="Bank Name"
                                                            type="text"
                                                            maxLength="10"
                                                            className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.bank ? <div className="text-danger">
                                                                    {errors.bank}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <label>Contact No</label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            {/* <textarea placeholder="Contact No" className="form-control form-control-sm" name="contact"></textarea> */}
                                                            <input
                                                                name="phone_no"
                                                                value={form['phone_no']}
                                                                onChange={this.handleChange}
                                                                placeholder="Contact No / Phone No"
                                                                type="text"
                                                                maxLength="10"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.phone_no ? <div className="text-danger">
                                                                    {errors.phone_no}
                                                                </div> : ''
                                                            }

                                                            <input
                                                                name="mobile"
                                                                value={form['mobile']}
                                                                onChange={this.handleChange}
                                                                placeholder="Mobile No "
                                                                type="text"
                                                                maxLength="10"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.mobile ? <div className="text-danger">
                                                                    {errors.mobile}
                                                                </div> : ''
                                                            }
                                                        </div>

                                                        <div className="col-md-2">
                                                            <label>Account Holder Name</label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            {/* <input placeholder="Bank A/C No." type="text" className="form-control form-control-sm" /> */}
                                                            <input
                                                                name="account_name"
                                                                value={form['account_name']}
                                                                onChange={this.handleChange}
                                                                placeholder="Account Name"
                                                                type="text"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.account_name ? <div className="text-danger">
                                                                    {errors.account_name}
                                                                </div> : ''
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-2">
                                                        <label>Office Address</label>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        {/* <input placeholder="Office Address" type="text" className="form-control form-control-sm" /> */}
                                                        <textarea
                                                                name="address"
                                                                value={form['address']}
                                                                onChange={this.handleChange}
                                                                placeholder="Office Address"
                                                                className="form-control form-control-sm"
                                                                name="address"
                                                            ></textarea>
                                                            {
                                                                errors.address ? <div className="text-danger">
                                                                    {errors.address}
                                                                </div> : ''
                                                            }
                                                    </div>

                                                    <div className="col-md-2">
                                                        <label>IFSC Code</label>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        {/* <input placeholder="IFSC Code" type="text" className="form-control form-control-sm" /> */}
                                                        <input
                                                            name="ifsc"
                                                            value={form['ifsc']}
                                                            onChange={this.handleChange}
                                                            placeholder="IFSC Code"
                                                            type="text"
                                                            maxLength="10"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.ifsc ? <div className="text-danger">
                                                                {errors.ifsc}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-2">
                                                        <label>Email Id</label>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        {/* <input placeholder="Email Id" type="text" className="form-control form-control-sm" /> */}
                                                        <input
                                                                name="email"
                                                                value={form['email']}
                                                                onChange={this.handleChange}
                                                                placeholder="Email ID"
                                                                type="text"
                                                                className="form-control form-control-sm"
                                                            />
                                                            {
                                                                errors.email ? <div className="text-danger">
                                                                    {errors.email}
                                                                </div> : ''
                                                            }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-2">
                                                        <label>Pan no</label>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        {/* <input placeholder="Pan no" type="text" className="form-control form-control-sm" /> */}
                                                        <input
                                                            name="pan_no"
                                                            value={form['pan_no']}
                                                            onChange={this.handleChange}
                                                            placeholder="Pan No"
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.pan_no ? <div className="text-danger">
                                                                {errors.pan_no}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-2">
                                                        <label>GST no</label>
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        {/* <input placeholder="Pan no" type="text" className="form-control form-control-sm" /> */}
                                                        <input
                                                            name="gst_no"
                                                            value={form['gst_no']}
                                                            onChange={this.handleChange}
                                                            placeholder="GST No"
                                                            type="text"
                                                            className="form-control form-control-sm"
                                                        />
                                                        {
                                                            errors.gst_no ? <div className="text-danger">
                                                                {errors.gst_no}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h3><b>Location</b></h3>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <label>State</label>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <label>Select District / City</label>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <label>Location</label>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="form-group col-md-4">
                                                        <select
                                                            name="state"
                                                            className="form-control form-control-sm"
                                                            onChange={this.getCity}
                                                            // defaultValue={state_id}
                                                        >
                                                            <option value="">Select Option</option>
                                                            {StateList}
                                                        </select>
                                                        {
                                                            errors.state ? <div className="text-danger">
                                                                {errors.state}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        <select
                                                            name="city"
                                                            onChange={this.getLocation}
                                                            className="form-control form-control-sm"
                                                        >
                                                            <option value="">Select Option</option>
                                                            {cityList}
                                                        </select>
                                                        {
                                                            errors.city ? <div className="text-danger">
                                                                {errors.city}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                    <div className="form-group col-md-4">
                                                        <select
                                                            name="location"
                                                            // onChange={this.handleChange}
                                                            className="form-control form-control-sm"
                                                        >
                                                            <option value="">Select Option</option>
                                                            {locationList}
                                                        </select>
                                                        {
                                                            errors.location ? <div className="text-danger">
                                                                {errors.location}
                                                            </div> : ''
                                                        }
                                                    </div>
                                                </div>
                                                
                                                <div className="row">
                                                    <div className="form-group col-md-4">
                                                        <button type="submit" className="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}

export default Profile
