import React, { Component } from 'react'
import Menu from "./Layouts/Menu";

export class About extends Component {
    render() {
        return (
            <div className="fluid-container">
                <Menu />
                <div className="clearfix"></div>
                <div className="container">
                    <div className="row">
                        <h1>About</h1>
                    </div>
                </div>
            </div>
        )
    }
}

export default About
