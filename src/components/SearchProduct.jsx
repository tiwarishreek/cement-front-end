import React, { Component } from 'react'

import { getState } from "../services/state";
import { getCity as getCityService } from "../services/city";
import { getMaterial } from "../services/material";
import { getType as getTypeService } from "../services/type";
import { searchProduct } from "../services/product";

import { getLocation as getLocationService } from "../services/location";

import ListProduct from "./ListProduct";
import { withRouter } from 'react-router-dom';

class SearchProduct extends Component {

    state = {
        states: [],
        cities: [],
        locations: [],

        materials: [],
        types: [],
        form: {
            city: '',
            location: '',
            type: '',
            customer: true
        },
        products: []
    }


    componentDidMount() {
        // let self = this;
        getState().then((response) => {
            // console.log(`\n>> States Response >\n ${JSON.stringify(response.data)}`);
            this.setState({ ...this.state, states: response.data.result });
        }).catch((error) => {
            console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
        })

        getMaterial().then((response) => {
            // console.log(`\n>> Material Response >\n ${JSON.stringify(response.data)}`);
            this.setState({ ...this.state, materials: response.data.result });
        }).catch((error) => {
            console.log(`\n>> Material Error >\n ${JSON.stringify(error)}`);
        })
    }

    getCity = (event) => {
        // console.log("city called", event.target.value);
        const stateId = event.target.value;
        getCityService(stateId).then((response) => {
            // console.log(`\n>>City Response >\n ${JSON.stringify(response.data)}`);
            this.setState({ ...this.state, cities: response.data.result });
        }).catch((error) => {
            console.log(`\n>>City Error >\n ${JSON.stringify(error)}`);
        })
    }

    getLocation = (event) => {
        const cityId = event.target.value;
        getLocationService(cityId).then((response) => {
            let { locations, form } = this.state;
            form = { ...form, city: cityId };
            locations = response.data.result;
            this.setState({ ...this.state, locations: locations, form: form });
        }).catch((error) => {
            console.log(`\n>>Location Error >\n ${JSON.stringify(error)}`);
        })
    }

    getType = (event) => {
        // console.log("type called", event.target.value);
        const materialId = event.target.value;
        getTypeService(materialId).then((response) => {
            // console.log(`\n>>Type Response >\n ${JSON.stringify(response.data)}`);
            this.setState({ ...this.state, types: response.data.result });
        }).catch((error) => {
            console.log(`\n>>Type Error >\n ${JSON.stringify(error)}`);
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        console.log("form submitted...");
        console.log("FORM - ", JSON.stringify(this.state.form));
        const { form } = this.state;
        searchProduct(form).then((response) => {
            this.setState({ ...this.state, products: response.data.result });
            console.log(`\n>>Search Product Response >\n ${JSON.stringify(this.state.products)}`);

            // this.setState({ ...this.state, types: response.data.result });
            // console.log(`\n>>Product State >\n ${JSON.stringify(this.state.products)}`);
        }).catch((error) => {
            console.log(`\n>>Type Error >\n ${JSON.stringify(error)}`);
        })

    }

    handleChange = (event) => {
        // console.log(`\n>>event>\n ${event.target.name} - ${event.target.value}`);
        var form = { ...this.state.form }
        form[event.target.name] = event.target.value;
        this.setState({ ...this.state, form })
        // this.setState({ ...this.state, form });
        // console.log("\n>>form >> \n", JSON.stringify(this.state.form));
    }

    render() {
        // console.log(`\n>>this.state>\n ${JSON.stringify(this.state)}`);
        const { states, cities, locations, materials, types, products } = this.state;

        const StateList = states.length > 0 ? states.map((state) => {
            return <option value={state._id} key={state._id}>{state.name}</option>
        }) : '';

        const cityList = cities.length > 0 ? cities.map((city) => {
            return <option value={city._id} key={city._id}>{city.name}</option>
        }) : '';
        const locationList = locations.length > 0 ? locations.map((location) => {
            return <option value={location._id} key={location._id}>{location.name}</option>
        }) : '';

        const materialList = materials.length > 0 ? materials.map((material) => {
            return <option value={material._id} key={material._id}>{material.name}</option>
        }) : '';

        const typeList = types.length > 0 ? types.map((type) => {
            return <option value={type._id} key={type._id}>{type.name}</option>
        }) : '';

        // const productList = products.length>0?

        return (
            <div className="portlet light">

                <div className="portlet-title">
                    <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                            Search Product
                        </span>
                    </div>
                </div>
                <div className="portlet-body form">
                    {/* <div className="col-offset-md-6"></div> */}
                    {/* <div className="col-md-6"> */}
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Select State</label>
                                <select className="form-control" onChange={this.getCity}>
                                    <option value="">Select Option</option>
                                    {StateList}
                                </select>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Select City</label>
                                <select 
                                    className="form-control" 
                                    name="city" 
                                    // onChange={this.handleChange}
                                    onChange={this.getLocation}
                                    >
                                    <option value="">Select Option</option>
                                    {cityList}
                                </select>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Select Location</label>
                                <select className="form-control" name="location" onChange={this.handleChange}>
                                    <option value="">Select Option</option>
                                    {locationList}
                                </select>
                            </div>
                        </div>
                        <div className="clearfix"></div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Select Material</label>
                                <select className="form-control" onChange={this.getType}>
                                    <option value="">Select Option</option>
                                    {materialList}
                                </select>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Select Type</label>
                                <select className="form-control" name="type" onChange={this.handleChange}>
                                    <option value="">Select Option</option>
                                    {typeList}
                                </select>
                            </div>
                        </div>




                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                    {/* </div> */}
                </div>
                <hr />

                <ListProduct products={products} />

            </div>
        )
    }
}

export default withRouter(SearchProduct);
