import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Layout from "./Layouts/Layout";
import { getMaterial } from "../services/material";
import { getType as getTypeService } from "../services/type";

import { postBrand as postBrandService } from "../services/brand";


class BrandForm extends Component {

  state = {
    message: '',
    error: false,
    errors: {
      material: '',
      type: '',
      name: ''
    },
    form: {
      material: '',
      type: '',
      name: ''
    },
    materials: [],
    types:[],
    processing: false
  }


  componentDidMount() {
    getMaterial().then((response) => {
      this.setState({ ...this.state, materials: response.data.result });
    }).catch((error) => {
      console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
    });
  }
  
  handleChange = (evt) => {
    let { error,errors, form } = this.state;
    const value = evt.target.value;
    if (!value) {
      error = true;
      errors = { ...errors, [evt.target.name]: `${evt.target.name} is required` };
    } else {
      error = false;
      errors = { ...errors, [evt.target.name]: '' };
    }
    form = { ...form, [evt.target.name]: value };
    this.setState({
      ...this.state,
      error,
      errors,
      form
    });
  }

  getType = (event) => {
    const materialId = event.target.value;
    let { form } = this.state;
    form = { ...form,material:materialId  };

    getTypeService(materialId).then((response) => {
      this.setState({ ...this.state, types: response.data.result, form:form });
    }).catch((error) => {
        console.log(`\n>>Type Error >\n ${JSON.stringify(error)}`);
    });
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    let { error, errors, form } = this.state;
    for (let key in form) {
      if (form.hasOwnProperty(key)) {
        let val = form[key];
        if (val === '' || val.length === 0) {
            error = true;
            errors[key] = `${key.replace('_', ' ')} field is required`;
        } else {
            errors[key] = '';
        }
      }
    }

    this.setState((prevState) => ({
      errors: errors,
      error: error,
    }));
    
    if (error == false) {
      postBrandService(this.state.form).then((response) => {
        this.props.history.push('/brands');
      }).catch((error) => {
        this.setState({
          ...this.state,
          message: error.response.data.message
        })
      })
    }
  }

  render() {

    const { error, errors, materials,types, form } = this.state;

    const materialList = materials.length > 0 ? materials.map((material) => {
        return <option value={material._id} key={material._id}>{material.name}</option>
    }) : '';

    const typeList = types.length > 0 ? types.map((type) => {
        return <option value={type._id} key={type._id}>{type.name}</option>
    }) : '';

    return (
      <React.Fragment>
        <Layout title="Brand Form">
          {/* <!-- BEGIN PAGE CONTENT --> */}
          <div className="page-content">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  {
                    this.state.message ? <div className="alert alert-danger alert-dismissible" >
                      {this.state.message}
                      <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div> : ''
                  }

                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          Brand Form
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body form">

                      {/* <div className="row">
                        {JSON.stringify(this.state.form, null, 4)}
                      </div> */}
                      <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                          <label>Select Material</label>
                          <select
                              name="material"
                              value={form['material']}
                              onChange={this.getType}
                              className="form-control form-control-sm"
                          >
                              <option value="">Select Option</option>
                              {materialList}
                          </select>

                          {
                              errors.material ? <div className="text-danger">
                                  {errors.material}
                              </div> : ''
                          }
                        </div>
                        <div className="form-group">
                          <label>Select Type</label>
                          <select
                              name="type"
                              value={form['type']}
                              onChange={this.handleChange}
                              className="form-control form-control-sm"
                          >
                              <option value="">Select Option</option>
                              {typeList}
                          </select>
                          {
                              errors.type ? <div className="text-danger">
                                  {errors.type}
                              </div> : ''
                          }
                        </div>

                        
                        <div className="form-group">
                          <label>Name</label>
                          <input
                            name="name"
                            value={form.name}
                            onChange={this.handleChange}
                            type="text"
                            className="form-control"
                            placeholder="Enter Name" />
                          {
                            errors.name ? <div className="text-danger">{errors.name}</div> : ''
                          }
                        </div>

                        <button type="submit" className="btn btn-primary">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

export default withRouter(BrandForm);
