import React from 'react';
import Layout from "./Layouts/Layout";

export default function Terms() {
  return (
    <div>
      <Layout title="Terms & Condition" >
        <div className="page-content">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="portlet light">
                            <div className="portlet-title">
                                <div className="caption">
                                    <i className="fa fa-cogs font-green-sharp"></i>
                                    <span className="caption-subject font-green-sharp bold uppercase">
                                      Terms & Condition
                                    </span>
                                </div>
                            </div>
                            <div className="portlet-body">
                            <p>
    <strong>Terms of use:</strong>
</p>
<p>
    Welcome to <strong>Hippo cement</strong>. We are so glad you’re here. Make
    yourself comfortable and have a good time, but please, follow house rules.
</p>
<ol>
    <li>
        <p>
            Accepting These Terms.
        </p>
    </li>
    <li>
        <p>
            Those Other Documents we mentioned.
        </p>
    </li>
    <li>
        <p>
            Community Policy.
        </p>
    </li>
    <li>
        <p>
            Privacy Policy.
        </p>
    </li>
    <li>
        <p>
            Hate speech policy.
        </p>
    </li>
    <li>
        <p>
            Review Policy.
        </p>
    </li>
    <li>
        <p>
            Cancellation/Return policy.
        </p>
    </li>
    <li>
        <p>
            Fees and Payment Policy.
        </p>
    </li>
    <li>
        <p>
            Delivery Policy.
        </p>
    </li>
    <li>
        <p>
            Your account with Hippo Cement.
        </p>
    </li>
    <li>
        <p>
            Your content.
        </p>
    </li>
    <li>
        <p>
            Your use of our services.
        </p>
    </li>
    <li>
        <p>
            Termination.
        </p>
    </li>
    <li>
        <p>
            Warranties and limitation of liability (or the things you can’t sue
            us for)
        </p>
    </li>
    <li>
        <p>
            Indemnification(Or what Happen if you get sued)
        </p>
    </li>
    <li>
        <p>
            Disputes with other users.
        </p>
    </li>
    <li>
        <p>
            Disputes with Hippo Cement.
        </p>
    </li>
    <li>
        <p>
            Changes to the Terms.
        </p>
    </li>
    <li>
        <p>
            Some finer legal point.
        </p>
    </li>
    <li>
        <p>
            Quality of Material
        </p>
    </li>
    <li>
        <p>
            Contact Information.
        </p>
    </li>
</ol>
<ol>
    <li>
        <p>
            <strong>Accepting These Terms.</strong>
        </p>
    </li>
</ol>
<p>
    This document and the other documents that we reference below make up our
    house rules, or what we officially call our Terms of Use (The “Terms” for
    short).
</p>
<p>
    The Terms are legally binding contract between you and Hippo Cement. This
    contract is only valid for who lives in India. Addition with it, follows
    all Indian penal code laws as per following list of laws.
</p>
<ol>
    <li>
        <p>
            Information technology Act-2000 (Central laws).
        </p>
    </li>
    <li>
        <p>
            Consumer Protection Act/Disputes resolution.
        </p>
    </li>
    <li>
        <p>
            National E-commerce Policy
        </p>
    </li>
    <li>
        <p>
            General Data Protection Regulation (GDPR)
        </p>
    </li>
    <li>
        <p>
            Payment and settlement system Act-2007 and Other RBI regulation on
            Payment mechanisms.
        </p>
    </li>
    <li>
        <p>
            Labeling and packaging.
        </p>
    </li>
    <li>
        <p>
            Legal Metrology Act-2009.
        </p>
    </li>
    <li>
        <p>
            GST Act.
        </p>
    </li>
</ol>
<p align="justify">
    Please note that, Disputes with Hippo Cement, contain an arbitration clause
    and class action waiver. By agreeing to the terms, You agree to resolve all
    disputes through binding individual arbitration which means that you waive
    any right to have those disputes decided by a judge or jury, and that you
    waive your right to participate your arbitration or representative actions.
</p>
<p align="justify">
    This contract sets out your rights and responsibilities when you use “Hippo
    Cement.com” , Pattern by Hippo Cement, Our mobile apps, and the other
    services provided by Hippo Cement(we’ll refer to all those collectively as
    our “Service”),so, please read it carefully. By using any of our services
    (even just browsing one of our websites), you are agreeing to the Terms. If
    you don’t agree with the Terms, You may not use our services. You with us?
    Great read on!
</p>
<ol start="2">
    <li>
        <p align="justify">
            <strong>Those other Documents we mentioned.</strong>
        </p>
    </li>
</ol>
<p align="justify">
    Hippo Cement services connect people in all around the India, both online
    and offline, to make sell and buy materials, Here’s a handy guide to help
    you understand the specific rules that relevant for you, depending on how
    use the services.
</p>
<p align="justify">
    2.11) Hippo Cement is a market place where you can sell your building
    material (cement) as per your dealership location area in India.
</p>
<p align="justify">
    2.12) Seller should be a dealer in cement manufacturer company.
</p>
<p align="justify">
    2.13) Seller should be sale cement on direct billing system (on billing
    name of cement manufacturer company only). They would be provided complete
    bank detail of cement manufacturer company.
</p>
<p align="justify">
    2.14) Seller should be provide complete details in seller registration form
    , online in Hippo Cement web site.
</p>
<p align="justify">
    2.15) Seller should be update price of Cement in seller account with
    validation period of cement price. After the validation period, Product
    listing is automatically removed from online selling portal. Seller should
    be updating the price on or before the validation period.
</p>
<p align="justify">
    2.16) Hippo Cement will update the price of material with 3% extra charges
    (internet payment charges and commission) which can be modified by Hippo
    Cement in future. Seller will get only amount as per price mention in
    seller’s account only with GST.
</p>
<p align="justify">
    2.17) Seller will get payment after 7(seven) days from the date of delivery
    of the material. The payment will pay to billing account only after the
    settle the charges, damages or lesser quantity receive at site.
</p>
<p align="justify">
    2.18) Seller may sale all types of cement as (1) OPC (2) PPC (3) Slag
    cement.
</p>
<p align="justify">
    2.19) The rate of material which is mentioned by you in seller account is
    inclusive of loading , unloading ,transportation, GST, Local taxes if any
    or any other charges.
</p>
<p align="justify">
    2.20) Seller should be provide a NOC for selling of material (i.e. cement)
    on Hippocement.com, from your manufacturer company and it upload on “seller
    registration”.
</p>
<p align="justify">
    <strong>2.2) House Rules for Buyers:-</strong>
</p>
<p align="justify">
    Hippo Cement is a venue where you can purchase building material like
    Cement and others from sellers in India. Whether you are looking for cement
    and other building material for suppliers, we want give you to have
    positive experience shopping on Hippo cement. Please, read on to find out
    more about your rights, as well as what is expected of you, as a buyers.
</p>
<p align="justify">
    This policy is a part of our Terms of use. By purchase on Hippo Cement,
    you’re agreeing to this policy and our Terms of Use.
</p>
<p align="justify">
    2.21) Hippo Cement does not pre-screen seller and therefore does not
    guarantee or endorse any items sold on Hippo Cement or any content posted
    by seller (such as photograph or language in listing).
</p>
<p align="justify">
    2.22) Buyers should be pay advance payment of material with internet
    charges and commission using appropriate payment option on Hippo Cement.
</p>
<p align="justify">
    2.23) Order of material is confirmed only after the received full payment
    of material in advance.
</p>
<p align="justify">
    2.24) Material (cement) will be dispatched within 7(seven) days from the
    date of purchase order.
</p>
<p align="justify">
    2.25) you are agree that in any circumstances no extra charges or interest
    will pay on your payment for 7(seven) days or any delay in delivery of
    material by seller.
</p>
<p align="justify">
    2.26) when you buy from a shop on Hippo Cement, You are directly supporting
    an independent business, each its unique listings polices; processing’s
    time, and payment system. By making a purchase from seller on Hippo Cement,
    You agree that you have,
</p>
<p align="justify">
    (a) Read the item description and shop policies before making a purchase.
</p>
<p align="justify">
    (b) Submitted appropriate payment for item(s) purchased;
</p>
<p align="justify">
    2.27) Hippo Cement’s Term of use requires all account owners to be at least
    18 years of age. Minor under 18 of age are not permitted to use the
    service.
</p>
<p align="justify">
    2.28) On occasion, Hippo Cement will need contact you, primarily, these
    messages are delivered by Email, Hippo Cement messages, for variety of
    reasons, including marketing, transaction, and services update purposes. If
    you no longer wish to receive notification, you can disable them at device
    level. You can opt out of receiving marketing communications via email or
    messages.
</p>
<p align="justify">
    2.29) Hippo Cement is a service provider to make a platform to buy and
    purchase of building material like cement. Hippo cement has not check or
    verifies the GST no of seller. So, in future any claim or GST refund money,
    Hippo cement is not directly or indirectly responsible for it. Tax invoice
    of material will be provide with the delivery of material and commission
    bill of service provider will provide by currier to you billing address.
</p>
<p align="justify">
    <strong>2.3) House Rules For Third Parties:-</strong>
</p>
<ol>
    <li>
        <p align="justify">
            <strong>Intellectual property Policy</strong>
        </p>
    </li>
</ol>
<p align="justify">
    We comply with intellectual property laws and industry best practice in
    order to maintain the integrity of our creative market place. This
    intellectual property policy explain how we address allegation of
    infringement, how authorized parties proper notices of infringement
    regarding the content of market place and how Hippo Cement seller can
    respond when their listing or shops are affected by notice.
</p>
<p align="justify">
    <strong>2.31) Hippo Cement Role:-</strong>
</p>
<p align="justify">
    Hippo Cement is a market place comprised of individual third parties seller
    who run their own shops, create their own polices, and are responsible for
    their own inventory, and complying with the law. We provide a venue, but
    Hippo Cement does not manufacture goods, hold inventory, or shipment on
    behalf of seller. The content upload on Hippo Cement’s market place is
    generated by independent seller who is not employees, agents or
    representatives of Hippo Cement. Seller are responsible for ensuring they
    have all necessary rights to their content and that they are not infringing
    or violating any third party’s right by porting it.
</p>
<p align="justify">
    Hippo Cement reserves the right to disable any listing, shop, or account
    that we believe violates our Terms of use including this intellectual
    property policy. Hippo Cement also reserves the right to take action
    against abuses of Hippo Cement’s intellectual property policy or our Terms
    of use.
</p>
<p align="justify">
    <strong>2.32) Notices of intellectual property infringement:-</strong>
</p>
<p align="justify">
    Hippo Cement strives to respond quickly when we receive proper notice of
    intellectual property infringement by removing or disabling access to the
    allegedly infringing material. When Hippo Cement removes or disables access
    in response to a notice, Hippo cement makes a reasonable attempt to contact
    to affected member, Provide information about the notice and removal and in
    case of alleged copyright infringement, provide information about how to
    submit a counter notice. Hippo Cement may also provide a copy of the
    infringement notice including the name and email address of the reporting
    party to effected members.
</p>
<p align="justify">
    <strong>2.33) Counter notice:-</strong>
</p>
<p align="justify">
    In accordance with copy right act, Hippo Cement accepts the copy right
    notice only. When Hippo Cement receives a counter notice, We will provide a
    copy of the counter notice to the original complaining party. The removed
    material may be replaced or access to it may be restore 10 (ten) business
    days after the counter notice is processed, unless the copyright owner
    files an action seeking a court order against the allegedly infringing
    party and informs of this action.
</p>
<p align="justify">
    <strong>2.34) Repeat infringement:-</strong>
</p>
<p align="justify">
    Hippo Cement terminates selling privileges of members who are subjects to
    repeat or multiple notice of intellectual property infringement in
    appropriate circumstances and at Hippo Cement’s discretion. If we believe a
    member has attempted to open a new shop after termination of the initial
    account, we reserve the right to refuse all services to that members. These
    actions apply to any account we believe are associated with or operated by
    the affected members. Per our Terms of Use, Hippo Cement reserves the right
    to terminate account privileges at any time, for any reason and without
    advance notice.
</p>
<p align="justify">
    <strong>2.35) Notice withdraws:-</strong>
</p>
<p align="justify">
    Hippo Cement only accepts withdraws of infringement notice directly from
    authorized repetitive who submitted the claim. The withdrawal must clearly
    state that it is a formal withdrawal and sufficiently identify the member
    and/or material (such as by providing the username, and Hippo Cement
    listing URLS).
</p>
<p align="justify">
    Once Hippo Cement receives the formal withdrawal of notice of infringement,
    Hippo Cement makes reasonable attempt to contact both parties involved to
    confirm receipt from the party submitting the withdrawal and to inform the
    member affected by the withdrawal. Please note that infringements matters
    are received by case by case bases and withdrawals do not guarantee changes
    to members’ shop statues.
</p>
<p align="justify">
    <strong>3) Community Policy:-</strong>
</p>
<p align="justify">
    3.1) You can use
    <strong>
        info@<a href="mailto:hippocement@gmail.com">hippocement</a>
    </strong>
    <strong>.com</strong>
    or <strong>Hippo Cement messages</strong>(“Messages”) to communicate
    directly with your buyers/sellers or other ‘Hippo Cement” members. Messages
    are great way to ask questions about an items or an order.
</p>
<p align="justify">
    3.2) Messages /Email may not be used for harassing or abusing another
    members or otherwise violating law.
</p>
<p align="justify">
    3.3) Messages/Email may not be used for the sending unsolicited advertising
    or promotions, requests for donations or spam.
</p>
<p align="justify">
    3.4):-You can’t contact someone after they have explicitly asked you not
    to.
</p>
<p align="justify">
    3.5):-Any use of Hippo Cement Messages to harass other members is strictly
    prohibited.
</p>
<p align="justify">
    3.6):-Be honest and transparent about who you are. Don’t use a fake
    Identity.
</p>
<p align="justify">
    3.7):-Be respectful toward other members. Don’t use community spaces to
    publicity discharge a specific member shop or item.
</p>
<p align="justify">
    3.8):-Don’t use community tools or space to interfere with another member’s
    business.
</p>
<p align="justify">
    3.9) don’t spam. This includes unsolicited or duplicate posts or links to
    your shop, fundraisers, surveys, social media or other promotional content.
</p>
<p align="justify">
    3.10):-Don’t use community spaces to discuss coordinating pricing with
    other sellers.
</p>
<p align="justify">
    3.11):-Don’t use community spaces to harass other members. Similarly, don’t
    post content in community space that may promote, support, or glorify
    hatred.
</p>
<p align="justify">
    3.12):-Don’t publish or post threads of violence against other or promote
    or encourage other to engage in violence or illegal activity.
</p>
<p align="justify">
    3.13):-Don’t use community spaces to encourage other to violate Hippo
    Cement Policies.
</p>
<p align="justify">
    <strong>4) Privacy Policy:-</strong>
</p>
<p align="justify">
    We process your personal information to run our business and provide our
    users with the services by accepting our Terms Of Use (and in some
    jurisdictions, by knowledge this policy). You confirm that you have read
    and understand this policy, including how and why we use your information.
    If you don’t want us to collect or process your personal information in the
    ways described in this policy you should not use the services. We are not
    responsible for the content or the privacy policies or practices of any of
    our members, websites, hosted through pattern by Hippo Cement, or third
    party web sites and apps.
</p>
<p align="justify">
    By using our services, you’re also agreeing that we can process your
    information. Both Hippo Cement and seller process member’s personal
    information (for example buyers name, email address, and delivery address,
    GST no, Bank Detail) and therefore considered separate and independent
    controllers of buyers personal information under law. That means that each
    party is responsible for the personal information it process in providing
    the services. For example, if a seller accidently discloses buyer’s name
    and email address when fulfilling another buyers order, the seller, not
    Hippo cement will be responsible for that unauthorized disclosure.
</p>
<ul>
    <li>
        <p align="justify">
            Respect other member’s privacy, don’t share private or personal
            identifying information in public area of the site.
        </p>
    </li>
</ul>
<p align="justify">
    <strong>5) Hate speech Policy:-</strong>
</p>
<p align="justify">
    Hippo Cement connects thoughtful consumers in all over India with creative
    entrepreneurs. It’s an ecosystem where people of all background inspire
    each other and build relationships through making, selling, and buying
    better materials. We want everyone on Hippo Cement to feel safe and our
    priority is fostering an inductive environment. This policy explains the
    kind of behavior we prohibited on Hippo Cement to make sure we all have a
    positive experience.
</p>
<p align="justify">
    This policy is a part of our Terms of use. By Using Hippo Cement You are
    agreeing to this policy and our Terms of Use.
</p>
<p align="justify">
    If you think discrimination or hate speech has occurred on Hippo Cement,
    Please, report it by emailing “<strong>info@hippocement.com</strong>” and
    we will investigate.
</p>
<p align="justify">
    <strong>6) Review Policy:- </strong>
</p>
<p align="justify">
    Reviews are great way to learn about a seller’s item (or Hippo Cement
    services), help seller build strong reputation, or warn other buyers about
    poor experience.
</p>
<p align="justify">
    We reserve the right to remove reviews or photographs that violate our
    policies or Terms of Use.<strong> </strong>
</p>
<p align="justify">
    <strong>7) Cancellation or return policy:-</strong>
</p>
<p align="justify">
    7.1) only sellers may cancel transactions. Buyers may request that a seller
    cancel an order via Hippo Cement message /E mail. Note that all
    cancellation must comply with our policy.<strong> </strong>
</p>
<p align="justify">
    7.2):- Each seller has his or her own return policies, which should be
    outlined in their shop policies. Not all sellers accept returns.
</p>
<p align="justify">
    7.3) Buyers may reject the material at time of delivery of material if the
    material is not as per Indian standard norms after verifying with Engineer
    from material(cement) manufacturing company. However, you are agreeing to
    accept the material, if engineer pass the quality of the material with bare
    cost of delay or any charges.
</p>
<p align="justify">
    7.4):-Order will cancelled automatically by if delivery of material delay
    more than 7(seven ) days from the date of purchase order. Buyers payment
    will be refunded within 7(seven) days from the date of cancellation.
</p>
<ol start="8">
    <li>
        <p align="justify">
            <strong>Fees and Payment Policy</strong>
            :-
        </p>
    </li>
</ol>
<p align="justify">
    Hippo Cement payment s allows buyers and seller to use various form of
    payments on hippocement.com and services. On Hippo Cement payment, users
    can pay with and accept payment by credit card, debit card, some bank
    transfer services, Google pay all in their in Indian currencies.
</p>
<p align="justify">
    8.1) Buyers should have to pay total of price of material (including basic
    price of material, loading, unloading, transportation, GST, any other
    government charges,) + 3% (may vary) (including commission, internet
    transaction charges) in advance.
</p>
<p align="justify">
    <strong>9) Delivery Policy</strong>
    :-
</p>
<p align="justify">
    <strong>10) Your account with Hippo Cement</strong>
    :-
</p>
<p align="justify">
    You will need to create an account with Hippo Cement to use some of our
    services. Here are a few rules about account with Hippo Cement.
</p>
<p align="justify">
    10.1):-You must be 18 years or older to use our services.
</p>
<p align="justify">
    10.2) Provide accurate information about your-self. It’s prohibited to use
    false information or impersonate another person or company through your
    account.
</p>
<p align="justify">
    10.3):-Choose and appropriate name. If you decide to not have your full
    name serve as the same associated with your account, you may not use
    language that is offensive, vulgar, infringes someone’s intellectual
    property right or otherwise violates the Terms.
</p>
<p align="justify">
    10.4) you are responsible for your account. You are solely responsible for
    any activity on your account. If you are sharing an account with any other
    people, then the person whose financial information is on the account will
    ultimately be responsible for all activities. If you are registering as a
    business entity, you personally grantee that you have the authority to
    agree to the Terms on behalf of the business. Also, your account is not
    transferable.
</p>
<p align="justify">
    10.5):-Protect your password:-As we mentioned above, you are solely
    responsible for any activity on your account, so it’s important to keep
    your account-password secure.
</p>
<p align="justify">
    10.6):-Let’s be clear about our relationship.
</p>
<p align="justify">
    These Terms don’t create any agency partnership, joint venture, employment,
    or franchisee relationship between you and Hippo Cement.
</p>
<p align="justify">
    10.7):-Hippo Cement can change, modified, edit or any, in terms of name,
    address, GST detail, Bank Detail, Rate of material, Type of material as per
    information received from the sellers.
</p>
<p align="justify">
    <strong>11) Your Content:-</strong>
</p>
<p align="justify">
    Content that you post using our services is your content (so let’s refer to
    it as “your content”). We don’t make any claim to it which includes
    anything you post using our services (like shop name, profile pictures,
    listing photos, listing description, reviews, comments, videos, usernames
    etc.)
</p>
<p align="justify">
    <strong>11.1) Responsibility for your content.</strong>
</p>
<p align="justify">
    You understand that you are solely responsible for your content. You
    represent that you have all necessary right to your content and that you’re
    not infringing or violating any third party right by posting it.
</p>
<p align="justify">
    <strong>11.2) Permission to use your content.</strong>
</p>
<p align="justify">
    By posting your content through our services you grant Hippo Cement a
    license to use it. We don’t claim any ownership to your content, but we
    have your permission to use it to help Hippo Cement function and grow. That
    way, we won’t infringe and right you have in your content and we can help
    promote your stuff for example, you acknowledge and agree Hippo Cement may
    after you or Hippo Cement buyer’s promotions on the site, from time to time
    that may relate to your listings.
</p>
<p align="justify">
    <strong>11.3)</strong>
    Hippo Cement can use your content for advertise purpose in buyer’s portal.
    And can resize photos for good looks, also can change language.
</p>
<p align="justify">
    You can use or send link of Hippo Cement to buyer of material for
    advertising/marketing purpose.
</p>
<p align="justify">
    <strong>11.4): Reporting unauthorized content.</strong>
</p>
<p align="justify">
    Hippo Cement has great respect for intellectual property right, and is
    committed to following appropriate legal procedures to remove infringing
    content from the services. If content that you owner have rights to has
    been posted to the services without your permission and you want it
    removed. If your content is alleged to infringe another person’s
    intellectual property, we will take appropriate action, such as disabling
    it if we receive proper notice or terminating your content if you are found
    to be a repeat infringer. We will notify you if any of that happens.
</p>
<p align="justify">
    <strong>11.5): Inappropriate, false, or misleading content.</strong>
</p>
<p align="justify">
    This should common sense, but there are certain type of content we don’t
    want posted on Hippo Cement’s services (for legal reason or otherwise). You
    agree that you will not post any content that is abusive, threatening,
    defamatory, obscene, vulgar, or otherwise offensive or in violation of our
    Terms. You also agree not to post any content that is false and misleading
    or uses the services in a manner that is fraudulent or deceptive.
</p>
<p align="justify">
    <strong>12): Your use of our services:- </strong>
</p>
<p align="justify">
    License to use our services we grant you a limited, non-exclusive,
    non-transferable, and revocable license to use our services subject to the
    Terms and the following restriction in particulars.
</p>
<p align="justify">
    <strong>12.1): Don’t use our services to break the law.</strong>
</p>
<p align="justify">
    You are agreeing that you will not violate any law in connection with your
    use of the services. This includes any local state, federal, and any other
    laws that apply to you.
</p>
<p align="justify">
    You may not engage in fraud (including claims of GST or Infringement
    notices)theft, anti-competitive conduct, threatening conduct, or any other
    unlawful acts or crimes against Hippo Cement, another Hippo Cement Users,
    or third party.
</p>
<p align="justify">
    <strong>12.2): Pay Your Bills.</strong>
</p>
<p align="justify">
    Your are responsible for paying all fees that you owe to Hippo Cement.
    Except as set forth below, you are also solely responsible for collecting
    and/or paying any applicable lexes for any purchases or sales you make
    through our services.
</p>
<p align="justify">
    Displayed rate are inclusive of all material basic rate, GST,
    transportation, loading, unloading and additional 3%( it may vary in
    future)charges of internet transaction charges and commission etc.
</p>
<p align="justify">
    <strong>12.3): Don’t steal our stuff.</strong>
</p>
<p align="justify">
    You agree not to crawl, scrape, or spider any page of the services or to
    reverse engineer or attempt to obtain source code of the services.
</p>
<p align="justify">
    <strong>12.4):Don’t try to harm our systems.</strong>
</p>
<p align="justify">
    You agree not to interfere with or try to disrupt our services, for example
    by distributing a virus or other harmful computer code.
</p>
<p align="justify">
    <strong>12.5): Follow our Trademark policy.</strong>
</p>
<p align="justify">
    The name Hippo Cement and the other Hippo Cement marks, phrases, logos, and
    design that we use in connection with our services, are tread marks,
    service marks, or trade dress of Hippo Cement in India.
</p>
<p align="justify">
    <strong>12.6): Share your ideas.</strong>
</p>
<p align="justify">
    We love your suggestions and ideas. They can help us improve your
    experience and our services. Any unsolicited idea or other materials you
    submit to Hippo Cement (not including our content or items you sell through
    our services) are considered non-confidentive and non-proprietary to you.
    You grant us a non-exclusive, worldwide, royalty-free, irrevocable,
    sub-licensable, perpetual license to use and publish those ideas and
    materials for any purpose, without compensation to you.
</p>
<p align="justify">
    <strong>12.7): Talk to us online.</strong>
</p>
<p align="justify">
    From time to time, Hippo Cement will provide you with certain legal
    information in writing. We can send you information electronically (such as
    by email) instead of mailing you paper copies (it’s better for the
    environment) and that your electronic agreement is the same as your
    signature on paper.
</p>
<p align="justify">
    <strong>13): Termination:-</strong>
</p>
<p align="justify">
    <strong>13.1):</strong>
    We’d hate to see you go, but you may terminate your account with Hippo
    Cement at any time from your account settings. Terminating your account
    will not affect the availability of some of your content that you posted
    through the services prior to termination. Oh, and you’ll still have to pay
    any outstanding bills.
</p>
<p align="justify">
    <strong>13.2): Termination by Hippo Cement: - </strong>
    We may terminate or suspend your account (and any related accounts) and
    your access to the services at any time, for any reason, and without
    advance notice if we do so, it’s important to understand that you don’t
    have a contractual or legal right to continue to use our services, for
    example, to sell or buy on our website to anyone, at any time for any
    reason.
</p>
<p align="justify">
    If you or Hippo Cement terminates your account, you may lose any
    information associated with your account including your content.
</p>
<p align="justify">
    <strong>13.3): We may discontinue the services.</strong>
</p>
<p align="justify">
    Hippo Cement reserves the right to change, suspend, or discontinue any of
    the services at any time, for any reason. We will not be liable to you for
    the effect that any changes to the services may have on you, including your
    income or your ability to generate revenue through the services.
</p>
<p align="justify">
    <strong>13.4): Survival:- </strong>
</p>
<p align="justify">
    The Terms will remain in effect even after your access to the service is
    terminated, or your use of the service ends.
</p>
<p align="justify">
    <strong>
        14): Warranties and limitation of liability (or the Things you can’t
        sue us for)
    </strong>
</p>
<p align="justify">
    <strong>14.1); Items you Purchase.</strong>
</p>
<p align="justify">
    You understand that Hippo Cement does not manufacture, store or inspect any
    of the items sold through our services. We provide the venue, the items in
    your marketplaces are produced, listed, so Hippo Cement can’t and doesn’t
    make any warranties about their quality, safety, or even their legality.
    Any legal claim related to an item you purchase must be brought directly
    against the seller of the item. You release Hippo Cement from any claim
    related to items sold through our services, including for defective items,
    misrepresentation by sellers, or items that accused physical injury(like
    product liability claims).
</p>
<p align="justify">
    <strong>14.2): Content you access.- </strong>
</p>
<p align="justify">
    You may come across materials that you find offensive or inappropriate
    while using our services. We make no representations concerning any content
    posted by users through the services. Hippo Cement is not responsible for
    the accuracy, copyright compliance, legality, or decency of content posted
    by users that you accessed through the services. You release us from all
    liability relating to that content.
</p>
<p align="justify">
    <strong>14.3): People you interact With.-</strong>
</p>
<p align="justify">
    You can use the services to interact with other individuals, either online
    or in person. However, you understand that we don’t screen users of our
    services and you release us from all liability relating to your interaction
    with other users. Please be careful and exercise caution and good judgment
    in all interaction with others especially if you are meeting someone in
    person.
</p>
<p align="justify">
    <strong>14.4): Third party Services.-</strong>
</p>
<p align="justify">
    Our services may contain links to third party web site or services that we
    don’t own or control (for example, links to face book, twitter). You may
    also need to use a third party’s product or service in order to use some of
    our services (like a compatible mobile device to use our mobile apps)When
    you access these third-party services, you do so at your own risk. The
    third parties may require you to accept their own terms of use. Hippo
    Cement is not a party to those agreements; they are solely between you and
    the third party.
</p>
<p align="justify">
    <strong>14.5): Gift cards and Promotions.-</strong>
</p>
<p align="justify">
    You acknowledge that Hippo Cement does not make any warranties with respect
    to your gift and balance and is not responsible for any unauthorized access
    to, or alteration, theft, or destruction of a gift and or gift card code
    that result from any action by you or a third party. You also acknowledge
    that we may suspend or prohibit use of your gift card if your gift card or
    gift card code has been reported lost or stolen , or if we believe your
    gift card balance is being used suspiciously, fraudently, or in an
    otherwise unauthorized manner.
</p>
<p align="justify">
    <strong>14.6): WARRANTIES:-</strong>
</p>
<p align="justify">
    HIPPO CEMENT IS DEDICATED TO MAKING OUR SERVICES THE BEST THEY CAN BE,BUT
    WE’RE NOT PERFECT AND SOMETIME THINGS CAN GO WRONG. YOU UNDERSTAND THAT OUR
    SERVICES ARE PROVIDED “AS IS AND WITHOUT ANY KINDOF WARRANTY (EXPRESS OR
    IMPLIED). WE ARE EXPRESSLY DISCLAIMING ANY WARRANTIES OF TITLE,
    NON-INFRINGNMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE, AS
    WELL AS ANY WARRANTIES IMPLIED BY A COURSE OF PERFORMANCE COURSE OF DEALING
    OR USAGE OF TRADE.
</p>
<p align="justify">
    WE DO NOT GUARANTEE THAT: (1) THE SERVICES WILL BE SECURE OR AVAILABLE AT
    ANY PARTICULAR TIME OR LOCATION; (2):ANY DEFECTS OR ERRORS WILL BE
    CORRECTED; (3)THE SERVICES WILL BE FREE OF VIRUSES OR OTHER HARMFUL
    MATERIALS; OR (4)THE RESULTS OF USING THE SERVICES WILL MEET YOUR
    EXPECTATIONS. YOU USE THE SERVICES SOLELY AT YOUR OWN RISK. SOME
    JURIDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES, SO THE ABOVE
    LIMITATIONS MAY NOT APPLY TO YOU.
</p>
<p align="justify">
    <strong>14.7): LIABILITY LIMIT:-</strong>
</p>
<p align="justify">
    TO THE FULLEST EXTENT PERMITED BY LAW NEITHER HIPPO CEMENT , NOR OUR
    EMPLOYEES SHALL BE LIABLE TO YOU FOR ANY LOST PROFITS OR REVENUES, OR FOR
    ANY CONSEQUENTIAL INDOENTAL, INDIRECT, SPECIAL, OR PUNITIVE DAMAGES ARISING
    OUT OF OR INCNNECTION WITH THE SERVICES OR THESE TERMS. IN NO EVENT SHALL
    HIPPO CEMENTS AGGRIGATE LIABILITY FOR ANY DAMAGES EXCEED THE GREATER OF ONE
    HUNDRED (Rs100) RUPEES OR THE AMOUNT YOU PAID HIPPO CEMENTIN THE PAST
    TWELVE MONTHS. SOME JURIDICTIONS DO NOT ALLOW LIMITATIONS ON INCIDENTAL OR
    CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.
</p>
<p align="justify">
    <strong>15): Indemnification (or what happens if you get sued):-</strong>
</p>
<p align="justify">
    We hope this never happens, but if Hippo Cement gets sued because of
    something that you did, you agree to defend and indemnify us. That means
    you’ll defend Hippo Cement (including any of our employees)and hold us
    harmless from any legal claim or demand(including reasonable lawyer’s fees)
    that arises from your actions, your use(or misuse)of our services, your
    breach of the Terms, or your account’s infringement of someone else’s
    rights.
</p>
<p align="justify">
    We reserve the right to handle our legal defense however we see fit, even
    if you are indemnifying us in which case you agree to cooperate with us so
    we can execute our strategy.
</p>
<p align="justify">
    <strong>16): Disputes with other users. :-</strong>
</p>
<p align="justify">
    If you find yourself in a dispute with another user of Hippo Cement’s
    services or a third party, we encourage you to contact the other party and
    try to resolve the dispute amicably.
</p>
<p align="justify">
    <strong>16.1) Case system:-</strong>
</p>
<p align="justify">
    Buyers and sellers, who are unable to resolve a dispute related to a
    transaction on our websites or mobile apps, may participate in our case
    system. Hippo Cement will attempt to help you resolve disputes in good
    faith and based solely on our interpretation of our policies, in our sole
    discretion; We will not make judgments regarding legal issues or claims.
    Hippo Cement has no obligation to resolve any disputes.
</p>
<p align="justify">
    <strong>16.2): Release of Hippo Cement:-</strong>
</p>
<p align="justify">
    You release Hippo Cement from any claim, demands, and damages arising out
    of disputes with other users or parties.
</p>
<p align="justify">
    <strong>17): Disputes with Hippo Cement:-</strong>
</p>
<p align="justify">
    If you’re upset with us, let us know, and hopefully we can resolve your
    issue. But if we can’t, then these rules will govern any legal dispute
    involving our services.
</p>
<p align="justify">
    <strong>17.1): Governing Law:</strong>
</p>
<p align="justify">
    The Terms are governed By Indian law and consumer protection law within
    Mumbai jurisdiction.
</p>
<p align="justify">
    <strong>17.2): Arbitration: </strong>
    You and Hippo Cement that any dispute or claim arising from or relating to
    the Terms shall be finally settled by final and binding arbitration, using
    the English language.
</p>
<p align="justify">
    Arbitration, including threshold questions of arbitrability of the dispute,
    will be handled by a sole arbitrator in accordance with those rules.
    Judgment on the arbitration award may be entered in any court that has
    jurisdiction. Any arbitration under the terms will take place on individual
    basis-class arbitration and class actions are not permitted. You and Hippo
    Cement are each waiving the right to trial by jury or to participate is a
    class action or class arbitration.
</p>
<p align="justify">
    <strong>17.3): Cost of Arbitration.:-</strong>
    Payment for any and all reasonable filling, administrative, and arbitrator
    fess will be in accordance with the consumer’ arbitration Rules.
</p>
<p align="justify">
    <strong>17.4): Forum.</strong>
</p>
<p align="justify">
    We’re based in Mumbai, India, so, any legal action against Hippo Cement
    related to our services must be filled and take place in Mumbai
    (State-Maharashtra).For any actions not subject to arbitration, you and
    Hippo Cement agree to submit to the personal jurisdiction of a state or
    federal court located in Mumbai(Maharashtra).
</p>
<p align="justify">
    <strong>17.5): Government Exception.</strong>
</p>
<p align="justify">
    If you are a government agent or entity in India using the services in your
    official capacity and you are legally unable to agree to the clause in this
    section, then those clause do not apply to you. In that case, the Terms and
    any action related to the Terms will be governed by the laws of
    India.(without reference to conflict of laws)
</p>
<p align="justify">
    <strong>17.6): Modifications:-</strong>
</p>
<p align="justify">
    If we make any changes to this “Disputes with Hippo Cement” section after
    the date you last accepts the Terms, those changes will not apply to any
    claim filed in a legal proceeding against Hippo Cement prior to the date
    the changes became effective. Hippo Cement will notify you of subtractive
    changes to the “Disputes with Hippo Cement” section at least 30 days prior
    to the date the change will become effective. If you do not agree to the
    modified terms, you may send Hippo Cement a written notification(including
    email) or close your account within those 30 days. By rejecting a modified
    terms or permanently closing your account, you agree to arbitrate any
    disputes between you and Hippo Cement in accordance with the provisions of
    this “ Disputes with Hippo Cement” section as of the date you last accepted
    the Terms, including any changes made prior to your rejection. If you
    reopen your closed account or create a new account, you agree to be bound
    by the current version of the Terms.
</p>
<p align="justify">
    <strong>18): Changes of the Terms:-</strong>
</p>
<p align="justify">
    We may update these Terms from time to time. If we believe that the changes
    are material, we will definitely let you know by posting the changes
    through the services and/or sending you an E mail or message about the
    changes. That way you can decide whether you want to continue using the
    services. Changes will be effective upon the positing of the changes unless
    otherwise specified. You are responsible for receiving and becoming
    familiar with any changes. You use of the services following the changes
    constitutes your acceptance of the update Terms.
</p>
<p align="justify">
    <strong>19): Some Finer Legal Points.- </strong>
</p>
<p align="justify">
    The terms, including all of the policies that make up the Terms, supersede
    any other agreement between you and Hippo Cement regarding the services. If
    any part of the Terms is found to be enforceable, that part will be limited
    to the minimum extent necessary so that the Term will otherwise remain in
    full force and effect. Our failure to enforce any part of the Terms is not
    waiver of our right to later enforce that or any other part of the Terms.
    We may assign any of our right and obligations under Terms.
</p>
<p align="justify">
    <strong>20): Quality of material:-</strong>
</p>
<p align="justify">
    <strong>20.1) Types and grade of cement: -</strong>
    The buyers have the option to buy for the following types and grade of
    cement.
</p>
<ol>
    <li>
        <p align="justify">
            IS 269:2015 Conforming ordinary Portland cement Grdae-43 (OPC-43G)
        </p>
    </li>
    <li>
        <p align="justify">
            IS 269:2015 Conforming Ordinary Portland cement Grade-53 (OPC-53 G)
        </p>
    </li>
    <li>
        <p align="justify">
            IS 1489:2015 CONFORMING Portland Pozzolana cement-(PPC)
        </p>
    </li>
</ol>
<p align="justify">
    Material (cement) for any of the above mentioned standardized grade shall
    be required to conform to Indian standard code. Other specification shall
    be conforming to the specification by the buyers.
</p>
<p align="justify">
    <strong>20.2): Size:- </strong>
    Material shall be delivered in packing of 50 kg with standard tolerance as
    mention in Indian standard code, as applicable.
</p>
<p align="justify">
    <strong>21): Contact Information:-</strong>
</p>
<p align="justify">
If you have any questions about the Terms, Please email us at    <strong>info@hippocement.com.</strong>
</p>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
    </div>
  )
}
