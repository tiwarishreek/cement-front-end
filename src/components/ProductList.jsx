import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Layout from "./Layouts/Layout";
import { searchProduct, deleteProduct } from "../services/product";
import auth from "../services/auth";

import sweetAlert from 'sweetalert2';

class ProductList extends Component {
  state = {
    message: '',
    processing: false,
    data: [],
    form:{
      dealer:'',
      customer:false,
      paginate:true,
      limit:10,
    }
  }

  constructor(props){
    super(props);
    let { form } = this.state;
    let role = auth.getAuth().user.user.role;
    if(role === 'Dealer'){
      form.dealer =  auth.getAuth().user.user._id;
      this.setState((prevState) => ({
        form: form
      }));
    }
  }

  editForm = (id) => {
    this.props.history.push({  
      pathname: '/product-form/edit/' + id  
    }); 
  }
  deleteForm = (id) => {
    sweetAlert.fire({
      title: 'Delete Product',
      text: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        deleteProduct(id).then((response) => {
          sweetAlert.fire(
            'Delete Product',
            'Product is deleted.',
            'success'
          );
          // this.props.history.push('/product-form');
          this.getData();
        }).catch((error) => {
            console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
            let msg = error.response.data.message;
            sweetAlert.fire(
              'Delete Product',
              msg,
              'error'
            );
        });
      } else if (result.dismiss === sweetAlert.DismissReason.cancel) {
        sweetAlert.fire(
          'Delete Product',
          'Cancelled',
          'error'
        )
      }
    })


  }
 
  componentDidMount() {
    this.getData();
  }

  getData = () => {
    let { form } = this.state;
    searchProduct(form).then((response) => {
      this.setState({ ...this.state, data: response.data.result });
    }).catch((error) => {
        console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
    });
  }

  getTable = (data) => {
    if(data.length>0){
      return data.map((student, index) => {
        const { _id, min_qty, dealer, brand, unit, price, stock } = student //destructuring
        return (
           <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ brand?.name ?? '-' }</td>
              <td>{ brand?.type?.material?.name ?? '-' }</td>
              <td>{ brand?.type?.name ?? '-' }</td>
              <td>{ unit ?? '-' }</td>
              <td>{ price ?? '-' }</td>
              <td>{ stock ?? '0'}</td>

              <td>
              <i className="fa fa-user"></i> 
                { dealer?.name ?? '-' } <br />
                <i className="fa fa-envelope"></i>
                { dealer?.email ?? '-' } <br />
                <i className="fa fa-phone"></i>

                { dealer?.mobile ?? '-' } <br />
                <i className="fa fa-address-book"></i>

                { dealer?.location?.name ?? '-' } <br />
                { dealer?.location?.city?.name ?? '-' } <br />
                { dealer?.location?.city?.state?.name ?? '-' } <br />
              </td>
              
              <td>
                <a className="btn default btn-xs purple" onClick={ () => { this.editForm(_id) }}>
                  <i className="fa fa-edit"></i> Edit
                </a>
                <a className="btn default btn-xs reed" onClick={ () => { this.deleteForm(_id) }}>
                  <i className="fa fa-trash-o"></i> Delete
                </a>
              </td>
           </tr>
        )
     })
    }else{
      return (<tr>
          <td colspan="4">
            No record found
          </td>
        </tr>);
    }
  }
  render() {

    const { processing, data, message } = this.state;
    const table = this.getTable(data);
    return (
      <React.Fragment>
        <Layout title="Product List" >
        <div className="page-content">
            <div className="container">

              <div className="row">
                <div className="col-md-12">
                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                        Product List
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body">
                      {/* <div className="form">
                        <form action="" className="form-horizontal">
                          <div className="form-body">
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label className="col-md-3 control-label">Small Input</label>
                                <div className="col-md-9">
                                  <input type="text" className="form-control input-sm" placeholder="Default Input" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label className="col-md-3 control-label">Small Input</label>
                                <div className="col-md-9">
                                  <input type="text" className="form-control input-sm" placeholder="Default Input" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label className="col-md-3 control-label">Small Input</label>
                                <div className="col-md-9">
                                  <input type="text" className="form-control input-sm" placeholder="Default Input" />
                                </div>
                              </div>
                            </div>
                            

                          </div>
                        </form>
                      </div> */}
                      <div className="table-scrollable">
                        <table className="table table-striped table-bordered table-advance table-hover">
                          <thead>
                            <tr>
                              <th>
                                #
                              </th>
                              <th>
                                Brand
                              </th>
                              <th>
                                Material
                              </th>
                              <th>
                                Type
                              </th>
                             
                              <th>
                                Unit
                              </th>
                              <th>
                                Price
                              </th>
                              <th>
                                Stock
                              </th>
                              <th>
                                Dealer Details
                              </th>
                              <th>
                                Action
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {table}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

export default withRouter(ProductList);
