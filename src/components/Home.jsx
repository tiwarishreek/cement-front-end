import React, { Component } from 'react';
import Menu from "./Layouts/Menu";
import Layout from "./Layouts/Layout";

import SearchProduct from "./SearchProduct";
class Home extends Component {

    state = {
        states: []
    };

    render() {
        return (
            <div>
                {/* <React.Fragment>
                    <Layout title="Search Product">
                        
                    </Layout>
                </React.Fragment> */}
                <Menu></Menu>
                <div className="page-container">
                    {/* <!-- BEGIN PAGE HEAD --> */}
                    <div className="page-head">
                        <div className="container">
                            {/* <!-- BEGIN PAGE TITLE --> */}
                            <div className="page-title">
                                <h1>Search Product <small>search product page</small></h1>
                            </div>
                            {/* <!-- END PAGE TITLE --> */}
                        </div>
                    </div>
                    {/* <!-- END PAGE HEAD --> */}
                    {/* <!-- BEGIN PAGE CONTENT --> */}
                    <div className="page-content">
                        <div className="container">
                            {/* <!-- BEGIN PAGE BREADCRUMB --> */}
                            <ul className="page-breadcrumb breadcrumb">
                                <li className="">
                                    <a>Home</a><i className="fa fa-circle"></i>
                                </li>
                                <li className="active">
                                    Search
				                </li>
                            </ul>
                            {/* <!-- END PAGE BREADCRUMB --> */}

                            <div className="row">
                                <div className="col-md-12">
                                    <SearchProduct />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- END PAGE CONTENT --> */}
                </div>

                <div className="page-footer">
                    <div className="container">
                        2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                    </div>
                </div>
                <div className="scroll-to-top">
                    <i className="icon-arrow-up"></i>
                </div>
            </div>
        )
    }
}

export default Home
