import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Layout from "./Layouts/Layout";
import { getAllLocation } from "../services/location";

class LocationList extends Component {
  state = {
    message: '',
    processing: false,
    data: []
  }

  componentDidMount() {
    getAllLocation().then((response) => {
      this.setState({ ...this.state, data: response.data.result });
    }).catch((error) => {
        console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
    });
  }

  getTable = (data) => {
    if(data.length>0){
      return data.map((student, index) => {
        const { _id, name, city } = student //destructuring
        return (
           <tr key={index}>
              <td>{index+1}</td>
              <td>{name}</td>
              <td>{ city.name }</td>
              <td>{ city.state.name }</td>
              <td>
                <span className="label label-sm label-success">
                  Approved 
                </span>
              </td>
              <td>
                <a className="btn default btn-xs purple">
                  <i className="fa fa-edit"></i> Edit 
                </a>
                <a className="btn default btn-xs reed">
                  <i className="fa fa-trash-o"></i> Delete 
                </a>
              </td>
           </tr>
        )
     })
    }else{
      return (<tr>
          <td colspan="4">
            No record found
          </td>
        </tr>);
    }
  }
  render() {

    const { processing, data, message } = this.state;
    const table = this.getTable(data);
    return (
      <React.Fragment>
        <Layout title="City List" >
        <div className="page-content">
            <div className="container">

              <div className="row">
                <div className="col-md-12">
                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          City List
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body">
                      <div className="table-scrollable">
                        <table className="table table-striped table-bordered table-advance table-hover">
                          <thead>
                            <tr>
                              <th>
                                #
                              </th>
                              
                              <th>
                                Location
                              </th>
                              <th>
                                City
                              </th>
                              <th>
                                State
                              </th>
                              <th>
                                Status
                              </th>
                              <th>
                                Action
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {table}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

export default withRouter(LocationList);
