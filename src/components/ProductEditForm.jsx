import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Layout from "./Layouts/Layout";

// import { getMaterial } from "../services/material";
// import { getType as getTypeService } from "../services/type";
// import { searchBrand } from "../services/brand";
import { updateProduct,searchProduct } from "../services/product";
import auth from "../services/auth";
import sweetAlert from 'sweetalert2';


class ProductEditForm extends Component {

  state = {

    message: '',
    error: false,
    errors: {
      material: '',

      dealer:'',
      type: '',
      brand: '',
      unit: '',
      price: '',
      stock: ''
    },
    form: {
      material: '',

      dealer:'',
      type: '',
      brand: '',
      unit: '',
      price: '',
      stock: ''
    },

    edit_data:'',
    processing: false
  }

  constructor(props){
    super(props);
    console.log("id");
    console.log(JSON.stringify(props.match.params.id));
  }


  componentDidMount() {
    let id = this.props.match.params.id;
    let { form,edit_data  } = this.state;

    searchProduct({id:id}).then((response) => {
      edit_data = response.data.result[0];

      form.dealer =  auth.getAuth().user.user._id;
      form.material = edit_data?.brand?.type?.material?._id ?? ''; 
      form.type = edit_data?.brand?.type?._id ?? ''; 
      form.brand = edit_data?.brand?._id ?? ''; 
      form.unit = edit_data.unit; 
      form.price = edit_data.price; 
      form.stock = edit_data.stock; 
      // auth.getAuth().user.user.name
      this.setState({ ...this.state, edit_data: edit_data,form:form });
    }).catch((error) => {
      console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
    });
  }
  
  handleChange = (evt) => {
    let { error,errors, form } = this.state;
    const value = evt.target.value;
    if (!value) {
      error = true;
      errors = { ...errors, [evt.target.name]: `${evt.target.name} is required` };
    } else {
      error = false;
      errors = { ...errors, [evt.target.name]: '' };
    }
    form = { ...form, [evt.target.name]: value };
    this.setState({
      ...this.state,
      error,
      errors,
      form
    });
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    let { error, errors, form } = this.state;
    for (let key in form) {
      if (form.hasOwnProperty(key)) {
        let val = form[key];
        if (val === '' || val.length === 0) {
            error = true;
            errors[key] = `${key.replace('_', ' ')} field is required`;
        } else {
            errors[key] = '';
        }
      }
    }

    this.setState((prevState) => ({
      errors: errors,
      error: error,
    }));
    
    if (error === false) {
      let id = this.props.match.params.id;
      updateProduct(this.state.form, id).then((response) => {
        sweetAlert.fire(
          'Update Product',
          'Product is Updated.',
          'success'
        );
        this.props.history.push('/products');
      }).catch((error) => {

        sweetAlert.fire(
          'Update Product',
          error.response.data.message,
          'error'
        );
        this.setState({
          ...this.state,
          message: error.response.data.message
        })
      })
    }
  }

  render() {

    const { errors,edit_data, form } = this.state;

    const materialList = (edit_data?.brand?.type?.material?._id) ?  <option value={edit_data?.brand?.type?.material?._id ?? ''} key={edit_data?.brand?.type?.material?._id ?? ''}>{edit_data?.brand?.type?.material?.name?? ''}</option> : '';

    const typeList = (edit_data?.brand?.type?.material?._id) ?  <option value={edit_data?.brand?.type?._id ?? ''} key={edit_data?.brand?.type?._id ?? ''}>{edit_data?.brand?.type?.name ?? ''}</option> : '';

    const brandList = (edit_data?.brand?._id) ?  <option value={edit_data?.brand?._id ?? ''} key={edit_data?.brand?._id ?? ''}>{edit_data?.brand?.name?? ''}</option> : '';

    return (
      <React.Fragment>
        <Layout title="Product Form">
          {/* <!-- BEGIN PAGE CONTENT --> */}
          <div className="page-content">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  {
                    this.state.message ? <div className="alert alert-danger alert-dismissible" >
                      {this.state.message}
                      <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div> : ''
                  }

                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          Product Form
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body form">

                      {/* <div className="row">
                        {JSON.stringify(this.state.form, null, 4)}
                      </div> */}
                      <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                          <label>Select Material</label>
                          <select
                              name="material"
                              value={form['material']}
                              onChange={this.getType}
                              className="form-control form-control-sm"
                              readonly
                          >
                              {materialList}
                          </select>

                          {
                              errors.material ? <div className="text-danger">
                                  {errors.material}
                              </div> : ''
                          }
                        </div>
                        <div className="form-group">
                          <label>Select Type</label>
                          <select
                              name="type"
                              value={form['type']}
                              onChange={this.getBrand}
                              className="form-control form-control-sm"
                              readonly
                          >
                              {typeList}
                          </select>
                          {
                              errors.type ? <div className="text-danger">
                                  {errors.type}
                              </div> : ''
                          }
                        </div>

                        <div className="form-group">
                          <label>Select Brand</label>
                          <select
                              name="brand"
                              value={form['brand']}
                              onChange={this.handleChange}
                              className="form-control form-control-sm"
                              readonly 
                          >
                              {brandList}
                          </select>
                          {
                              errors.brand ? <div className="text-danger">
                                  {errors.brand}
                              </div> : ''
                          }
                        </div>

                        
                        <div className="form-group">
                          <label>Unit</label>
                          <input
                            name="unit"
                            value={form.unit}
                            onChange={this.handleChange}
                            type="text"
                            className="form-control"
                            placeholder="Enter Unit" />
                          {
                            errors.unit ? <div className="text-danger">{errors.unit}</div> : ''
                          }
                        </div>
                        <div className="form-group">
                          <label>Price</label>
                          <input
                            name="price"
                            value={form.price}
                            onChange={this.handleChange}
                            type="number"
                            className="form-control"
                            placeholder="Enter Unit" />
                          {
                            errors.price ? <div className="text-danger">{errors.price}</div> : ''
                          }
                        </div>
                        <div className="form-group">
                          <label>Stock</label>
                          <input
                            name="stock"
                            value={form.stock}
                            onChange={this.handleChange}
                            type="number"
                            min="1"
                            className="form-control"
                            placeholder="Enter Unit" />
                          {
                            errors.stock ? <div className="text-danger">{errors.stock}</div> : ''
                          }
                        </div>

                        <button type="submit" className="btn btn-primary">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

export default withRouter(ProductEditForm);
