import React, { Component } from 'react'
import Menu from "./Layouts/Menu";
import Layout from "./Layouts/Layout";
import { getState } from "../services/state";

export class StateList extends Component {

  state = {
    message: '',
    processing: false,
    data: []
  }



  componentDidMount() {
    getState().then((response) => {
      this.setState({ ...this.state, data: response.data.result });
    }).catch((error) => {
        console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
    });
  }

  getTable = (data) => {




    if(data.length>0){
      return data.map((student, index) => {
        const { _id, name } = student //destructuring
        return (
           <tr key={index}>
              <td>{index+1}</td>
              <td>{name}</td>
              <td>
                <span className="label label-sm label-success">
                  Approved 
                </span>
              </td>
              <td>
                <a className="btn default btn-xs purple">
                      <i className="fa fa-edit"></i> Edit 
                </a>
                <a className="btn default btn-xs reed">
                      <i className="fa fa-trash-o"></i> Delete 
                </a>
              </td>
           </tr>
        )
     })
    }else{
      return (<tr>
          <td colspan="4">
            No record found
          </td>
        </tr>);
    }
  }
  render() {

    const { processing, data, message } = this.state;

      const table = this.getTable(data);
     

    return (

      <div>
        <Menu></Menu>
        <div className="page-container">
          {/* <!-- BEGIN PAGE HEAD --> */}
          <div className="page-head">
            <div className="container">
              {/* <!-- BEGIN PAGE TITLE --> */}
              <div className="page-title">
                <h1>State List
                  <small>
                    <i className="fa fa-plus"></i>
                    State List Page
                  </small>
                </h1>
              </div>
              {/* <!-- END PAGE TITLE --> */}
            </div>
          </div>

          {/* <!-- END PAGE HEAD --> */}
          {/* <!-- BEGIN PAGE CONTENT --> */}
          <div className="page-content">
            <div className="container">
              {/* <!-- BEGIN PAGE BREADCRUMB --> */}
              <ul className="page-breadcrumb breadcrumb">
                <li className="">
                  <a href="#">Home</a><i className="fa fa-circle"></i>
                </li>
                <li className="active">
                  State List
                              </li>
              </ul>
              {/* <!-- END PAGE BREADCRUMB --> */}

              <div className="row">
                <div className="col-md-12">
                  {
                    this.state.message ? <div className="alert alert-danger alert-dismissible" >
                      {this.state.message}
                      <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div> : ''
                  }

                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          State List
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body">
                      <div className="table-scrollable">
                        <table className="table table-striped table-bordered table-advance table-hover">
                          <thead>
                            <tr>
                              <th>
                                #
                              </th>
                              <th>
                                State
                              </th>
                              <th>
                                Status
                              </th>
                              <th>
                                Action
                              </th>
                             
                            </tr>
                          </thead>
                          <tbody>
                            {table}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="page-footer">
          <div className="container">
            2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
          </div>
        </div>
        <div className="scroll-to-top">
          <i className="icon-arrow-up"></i>
        </div>
      </div>

    )
  }
}

export default StateList
