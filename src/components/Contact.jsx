import React, { Component } from 'react'
import Layout from "./Layouts/Layout";

export default class Contact extends Component {
    render() {
        return (
            <React.Fragment>
                <Layout title="Contact Us" >
                    <div className="page-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="portlet light">
                                        <div className="portlet-title">
                                            <div className="caption">
                                                <i className="fa fa-cogs font-green-sharp"></i>
                                                <span className="caption-subject font-green-sharp bold uppercase">
                                                    Contact Us
                                                </span>
                                            </div>
                                        </div>
                                        <div className="portlet-body">
                                            Contact Us
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }


}

// export default About
