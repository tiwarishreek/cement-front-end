import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Layout from "./Layouts/Layout";
import { getState } from "../services/state";
import { getCity as getCityService } from "../services/city";
import { postLocation } from "../services/location";

class CityForm extends Component {

  state = {
    message: '',
    error: false,
    errors: {
      state: '',
      city:'',
      name: ''
    },
    form: {
      state: '',
      city:'',
      name: ''
    },
    states: [],
    cities: [],
    processing: false
  }



  componentDidMount() {
    getState().then((response) => {
      this.setState({ ...this.state, states: response.data.result });
    }).catch((error) => {
      console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
    });
  }

  getCity = (event) => {
    const stateId = event.target.value;
    getCityService(stateId).then((response) => {
        let { cities, form } = this.state;
        form = { ...form, state: stateId };
        cities = response.data.result;
        this.setState({ ...this.state, cities: cities, form: form });
    }).catch((error) => {
        console.log(`\n>>City Error >\n ${JSON.stringify(error)}`);
    })
}
  
  handleChange = (evt) => {
    let { error,errors, form } = this.state;
    const value = evt.target.value;
    if (!value) {
      error = true;
      errors = { ...errors, [evt.target.name]: `${evt.target.name} is required` };
    } else {
      error = false;
      errors = { ...errors, [evt.target.name]: '' };
    }
    form = { ...form, [evt.target.name]: value };
    this.setState({
      ...this.state,
      error,
      errors,
      form
    });
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    let { error, errors, form } = this.state;
    for (let key in form) {
      if (form.hasOwnProperty(key)) {
          let val = form[key];
          if (val === '' || val.length === 0) {
              error = true;
              errors[key] = `${key.replace('_', ' ')} field is required`;
          } else {
              errors[key] = '';
          }
      }
    }

    this.setState((prevState) => ({
      errors: errors,
      error: error,
    }));
    
    if (error == false) {
      postLocation(this.state.form).then((response) => {
        this.props.history.push('/locations');
      }).catch((error) => {
        this.setState({
          ...this.state,
          message: error.response.data.message
        })
      })
    }
  }




  render() {

    const { error, errors, states,cities, form } = this.state;

    const StateList = states.length > 0 ? states.map((state) => {
      return <option value={state._id} key={state._id}>{state.name}</option>
    }) : '';

    const cityList = cities.length > 0 ? cities.map((city) => {
        return <option value={city._id} key={city._id}>{city.name}</option>
    }) : '';


    return (

      <React.Fragment>
        <Layout title="Location Form">
          {/* <!-- BEGIN PAGE CONTENT --> */}
          <div className="page-content">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  {
                    this.state.message ? <div className="alert alert-danger alert-dismissible" >
                      {this.state.message}
                      <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div> : ''
                  }

                  <div className="portlet light">
                    <div className="portlet-title">
                      <div className="caption">
                        <i className="fa fa-cogs font-green-sharp"></i>
                        <span className="caption-subject font-green-sharp bold uppercase">
                          Location Form
                        </span>
                      </div>
                    </div>
                    <div className="portlet-body form">

                      {/* <div className="row">
                        {JSON.stringify(this.state.error, null, 4)}
                      </div>
                      <div className="row">
                        {JSON.stringify(this.state.errors, null, 4)}
                      </div> */}


                      <form onSubmit={this.handleSubmit}>
                      <div className="form-group">
                          <label>Select State</label>
                          <select
                            name="state"
                            className="form-control"
                            onChange={this.getCity}
                          >
                            <option value="">Select Option</option>
                            {StateList}
                          </select>
                          {
                            errors.state ? <div className="text-danger">
                              {errors.state}
                            </div> : ''
                          }
                        </div>
                        <div className="form-group">
                          <label>Select City</label>
                          <select
                              name="city"
                              onChange={this.handleChange}
                              className="form-control form-control-sm"
                          >
                              <option value="">Select Option</option>
                              {cityList}
                          </select>
                          {
                              errors.city ? <div className="text-danger">
                                  {errors.city}
                              </div> : ''
                          }
                        </div>


                        

                        <div className="form-group">
                          <label>Location</label>
                          <input
                            name="name"
                            value={form.name}
                            onChange={this.handleChange}
                            type="text"
                            className="form-control"
                            placeholder="Enter Name" />
                          {
                            errors.name ? <div className="text-danger">{errors.name}</div> : ''
                          }
                        </div>

                        <button type="submit" className="btn btn-primary">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

export default withRouter(CityForm);
