import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import CartService from "./../services/cart";
import AuthService from "./../services/auth";

class ListProduct extends Component {

    state = {
        states: []
    };

    buyClicked = (id) => {

        CartService.removeCart();
        CartService.setCart({
            id,
            qty: 1
        });

        let redirect = '/view-product';
        if (!AuthService.checkAuth()) {
            redirect = "/login";
        }
        if (AuthService.getRole() !== "Customer") {
            redirect = "/login";
            AuthService.logout();
        }

        this.props.history.push(redirect);
        return;
    }

    getTable = () => {
        let { products } = this.props;
        if (products.length > 0) {
            return products.map((product, index) => {
                const { _id, brand, unit, price, stock } = product //destructuring
                return (
                    <tr key={_id}>
                        <td>{index + 1}</td>
                        <td>{brand?.name ?? '-'}</td>
                        <td>{unit ?? '-'}</td>
                        <td>{price ?? '-'}</td>
                        <td>
                            <input type="button" value="Buy" className="btn btn-success" onClick={() => this.buyClicked(_id)} />
                        </td>
                    </tr>
                )
            })
        } else {
            return (<tr>
                <td colspan="5">
                    No record found
              </td>
            </tr>);
        }
    }

    render() {
        let { products } = this.props;

        return (
            <div>
                {products.length > 0 ?
                    <Fragment>
                        <div className="row">
                            {/* <h1>List Product</h1> */}
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold uppercase">List Product</span>
                                </div>
                            </div>
                        </div>
                        <hr />

                        <div className="table-scrollable">
                            <table className="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Brand</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.getTable()}
                                </tbody>
                            </table>
                        </div>
                    </Fragment> : ''
                }

            </div>
        )
    }
}

export default withRouter(ListProduct)
