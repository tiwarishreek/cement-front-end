import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Layout from "./Layouts/Layout";
import { postState } from "../services/state";

class StateForm extends Component {

    state = {
        message: '',
        error: false,
        errors: {
            name: ''
        },
        form: {
            name: ''
        },
        processing: false
    }


    handleChange = (evt) => {
        let { error, form } = this.state;
        const value = evt.target.value;
        if (!value) {
            error = true;
        }
        form = { ...form, [evt.target.name]: value }
        this.setState({
            ...this.state,
            error,
            form
        });
    }

    handleSubmit = (evt) => {
        evt.preventDefault();

        let { error, errors, form } = this.state;

        if (form.name == '') {
            error = true;
            errors.name = "Please enter name";
        } else {
            error = false;
            errors.name = "";
        }

        this.setState((prevState) => ({
            errors: errors,
            error: error,
        }));
        if (error == false) {
            postState(this.state.form).then((response) => {
                // console.log(JSON.stringify(response));
                this.props.history.push('/states');
            }).catch((error) => {
                this.setState({
                    ...this.state,
                    message: error.response.data.message
                })
            })
        }
    }



    render() {

        const { error, errors, form } = this.state;

        return (

            <React.Fragment>
                <Layout title="State Form">
                        {/* <!-- BEGIN PAGE CONTENT --> */}
                        <div className="page-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    {
                                        this.state.message ? <div className="alert alert-danger alert-dismissible" >
                                            {this.state.message}
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div> : ''
                                    }

                                    <div className="portlet light">
                                        <div className="portlet-title">
                                            <div className="caption">
                                                <i className="fa fa-cogs font-green-sharp"></i>
                                                <span className="caption-subject font-green-sharp bold uppercase">
                                                    State Form
                                                </span>
                                            </div>
                                        </div>
                                        <div className="portlet-body form">
                                            <form onSubmit={this.handleSubmit}>
                                                <div className="form-group">
                                                    <label>Name</label>
                                                    <input
                                                        name="name"
                                                        value={form.name}
                                                        onChange={this.handleChange}
                                                        type="text" 
                                                        className="form-control" 
                                                        placeholder="Enter Name" />
                                                        {
                                                            errors.name ? <div className="text-danger">{errors.name}</div> : ''
                                                        }
                                                </div>
                                                <button type="submit" className="btn btn-primary">Submit</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}

export default withRouter(StateForm);
