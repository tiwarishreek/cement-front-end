import React, { Component } from 'react'
import Menu from "./Layouts/Menu";

import { getState } from "../services/state";
import { getCity as getCityService } from "../services/city";
import { postUser as postUserService } from "../services/user";

export class Profile extends Component {

    state = {
        states: [],
        cities: [],
        locations: [],
        form: {
            name: '',
            email: '',
            mobile: '',
            password: '',
            city: '',
            location: '',
            address: '',
            pin_code: '',
            role: 'Dealer'
        },
        registration_error: '',
        user: ''
    }

    componentDidMount() {
        getState().then((response) => {
            this.setState({ ...this.state, states: response.data.result });
        }).catch((error) => {
            console.log(`\n>> States Error >\n ${JSON.stringify(error)}`);
        })
    }

    getCity = (event) => {
        // console.log("city called", event.target.value);
        const stateId = event.target.value;
        getCityService(stateId).then((response) => {
            // console.log(`\n>>City Response >\n ${JSON.stringify(response.data)}`);
            this.setState({ ...this.state, cities: response.data.result });
        }).catch((error) => {
            console.log(`\n>>City Error >\n ${JSON.stringify(error)}`);
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log("form submitted...");
        console.log("FORM - ", JSON.stringify(this.state.form));
        const { form } = this.state;
        postUserService(form).then((response) => {
            this.setState({ ...this.state, products: response.data.result });
            console.log(`\n>>Search Product Response >\n ${JSON.stringify(this.state.products)}`);
        }).catch((error) => {
            this.setState({
                ...this.state,
                registration_error: error.response.data.message
            });
            console.log(`\n>> Error >\n ${JSON.stringify(error.response)}`);
        })

    }

    handleChange = (event) => {
        // console.log(`\n>>event>\n ${event.target.name} - ${event.target.value}`);
        var form = { ...this.state.form }
        form[event.target.name] = event.target.value;
        this.setState({ ...this.state, form })
        // this.setState({ ...this.state, form });
        // console.log("\n>>form >> \n", JSON.stringify(this.state.form));
    }

    render() {


        const { states, cities, locations } = this.state;

        const StateList = states.length > 0 ? states.map((state) => {
            return <option value={state._id} key={state._id}>{state.name}</option>
        }) : '';

        const cityList = cities.length > 0 ? cities.map((city) => {
            return <option value={city._id} key={city._id}>{city.name}</option>
        }) : '';
        const locationList = locations.length > 0 ? locations.map((location) => {
            return <option value={location._id} key={location._id}>{location.name}</option>
        }) : '';

        return (


            <div>
                <Menu></Menu>
                <div className="page-container">
                    {/* <!-- BEGIN PAGE HEAD --> */}
                    <div className="page-head">
                        <div className="container">
                            {/* <!-- BEGIN PAGE TITLE --> */}
                            <div className="page-title">
                                <h1>Profile </h1>
                            </div>
                            {/* <!-- END PAGE TITLE --> */}
                        </div>
                    </div>
                    {/* <!-- END PAGE HEAD --> */}
                    {/* <!-- BEGIN PAGE CONTENT --> */}
                    <div className="page-content">
                        <div className="container">
                            {/* <!-- BEGIN PAGE BREADCRUMB --> */}
                            <ul className="page-breadcrumb breadcrumb">
                                <li className="">
                                    <a href="#">Home</a><i className="fa fa-circle"></i>
                                </li>
                                <li className="active">
                                    Seller Profile
				                </li>
                            </ul>
                            {/* <!-- END PAGE BREADCRUMB --> */}

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="portlet light">
                                        <div className="portlet-title">
                                            <div className="caption">
                                                <i className="fa fa-cogs font-green-sharp"></i>
                                                <span className="caption-subject font-green-sharp bold uppercase">
                                                    Seller Profile
                                                </span>
                                            </div>
                                        </div>
                                        <div className="portlet-body form">
                                            <form onSubmit={this.handleSubmit}>
                                                {/* <div class="card-header">Dealer Registration</div> */}
                                                <div class="card-body">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h3><b>Company Details</b></h3>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h3><b>Account Details</b></h3>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <label>Company Name</label>
                                                        </div>    
                                                        <div className="form-group col-md-4">
                                                            <input type="text" placeholder="Company Name" className="form-control form-control-sm" />
                                                        </div>
                                                        
                                                        <div className="col-md-2">
                                                            <label>Bank A/C</label>
                                                        </div>    
                                                        <div className="form-group col-md-4">
                                                            <input placeholder="Bank A/C" type="text" className="form-control form-control-sm" />
                                                        </div>
                                                    </div> 
                                                    <div className="row">   
                                                        <div className="col-md-2">
                                                            <label>Seller Name </label>
                                                        </div>    
                                                        <div className="form-group col-md-4">                                                                
                                                            <input placeholder="Seller Name" type="text" maxLength="10" className="form-control form-control-sm" />
                                                        </div>
                                                      
                                                        <div className="col-md-2">
                                                            <label>Bank Name </label>
                                                        </div>    
                                                        <div className="form-group col-md-4">                                                                
                                                            <input placeholder="Bank Name" type="text" maxLength="10" className="form-control form-control-sm" />
                                                        </div>
                                                    </div>   
                                                    <div className="row">  
                                                        <div className="col-md-2">
                                                            <label>Contact No</label>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            <textarea placeholder="Contact No" className="form-control form-control-sm" name="contact"></textarea>
                                                        </div>
                                                    
                                                        <div className="col-md-2">
                                                            <label>Bank A/C No.</label>
                                                        </div>    
                                                            <div className="form-group col-md-4">                                                                
                                                                <input placeholder="Bank A/C No." type="text" className="form-control form-control-sm" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">    
                                                        <div className="col-md-2">
                                                            <label>Office Address</label>
                                                        </div>    
                                                        <div className="form-group col-md-4">        
                                                                <input placeholder="Office Address" type="text" className="form-control form-control-sm" />
                                                        </div>                                                        
                                                    
                                                        <div className="col-md-2">
                                                            <label>IFSC Code</label>
                                                        </div>    
                                                        <div className="form-group col-md-4">        
                                                                <input placeholder="IFSC Code" type="text" className="form-control form-control-sm" />
                                                        </div>
                                                    </div>
                                                    <div className="row">    
                                                        <div className="col-md-2">
                                                            <label>Email Id</label>
                                                        </div>
                                                        <div className="form-group col-md-4">        
                                                            <input placeholder="Email Id" type="text" className="form-control form-control-sm" />
                                                        </div>                                                  
                                                    </div>
                                                    <div className="row">    
                                                        <div className="col-md-2">
                                                            <label>Pan no</label>
                                                        </div>    
                                                        <div className="form-group col-md-4">                                                                
                                                            <input placeholder="Pan no" type="text" className="form-control form-control-sm" />
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-2">
                                                            <label>GST no</label>
                                                        </div>    
                                                        <div className="form-group col-md-4">                                                                
                                                            <input placeholder="Pan no" type="text" className="form-control form-control-sm" />
                                                        </div>
                                                    </div>
                                                    
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h3><b>Location</b></h3>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-4">
                                                            <label>State</label>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <label>Select District / City</label>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <label>Location</label>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="form-group col-md-4">                                                                
                                                            <select className="form-control form-control-sm" onChange={this.getCity}>
                                                                <option value="">Select Option</option>
                                                                {StateList}
                                                            </select>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            <select className="form-control form-control-sm" name="city" onChange={this.handleChange}>
                                                                <option value="">Select Option</option>
                                                                {cityList}
                                                            </select>
                                                        </div>
                                                        <div className="form-group col-md-4">                                                                
                                                            <select className="form-control form-control-sm" name="location" onChange={this.handleChange}>
                                                                <option value="">Select Option</option>
                                                                {locationList}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="form-group col-md-4">
                                                            <button type="submit" className="btn btn-primary">Submit</button>
                                                        </div>
                                                    </div>                                                    
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="page-footer">
                    <div className="container">
                        2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                    </div>
                </div>
                <div className="scroll-to-top">
                    <i className="icon-arrow-up"></i>
                </div>
            </div>





        )
    }
}

export default Profile
