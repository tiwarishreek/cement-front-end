import axios from "axios";

import { HOST,setHeader } from "../config";
const headers = setHeader();

const path = "material";

export const getMaterial = () => {
    let url = HOST + path;
    return axios
        .get(url, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const postMaterial = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}