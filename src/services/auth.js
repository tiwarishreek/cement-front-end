const auth = {
    isAuthenticated: false,
    user: '',
    authenticate(user) {
        localStorage.setItem("user", JSON.stringify(user));
        localStorage.setItem("isAuthenticated", true);
        this.isAuthenticated = true;
        this.user = user;
    },
    logout() {
        localStorage.removeItem("user");
        localStorage.removeItem("isAuthenticated");
        this.isAuthenticated = false;
        this.user = '';
    },
    getAuth() {
        let user = localStorage.getItem("user");
        let isAuthenticated = localStorage.getItem("isAuthenticated");
        return {
            isAuthenticated: isAuthenticated,
            user: JSON.parse(user)
        };
    },

    checkAuth() {
        let isAuthenticated = localStorage.getItem("isAuthenticated");
        if(isAuthenticated === "true"){
            return true;
        }
        return false;
    },

    getAuthUser() {
        if (this.checkAuth() === true) {
            let user = localStorage.getItem("user");
            user = JSON.parse(user);
            return user.user;
        }
        return null;
    },

    getToken() {
        if (this.checkAuth() === true) {
            let user = localStorage.getItem("user");
            user = JSON.parse(user);
            return user.token;
        }
        return null;
    },

    getUserId() {
        let user = this.getAuthUser();
        if (user !== null) {
            return user._id;
        }
        return null;
    },



    getRole() {
        let user = this.getAuthUser();
        if (user !== null) {
            return user.role;
        }
        return null;
    }
};
export default auth;