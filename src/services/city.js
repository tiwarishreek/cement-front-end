import axios from "axios";

import { HOST, setHeader } from "../config";
const headers = setHeader();

const path = "city";

export const getAllCity = () => {
    let url = HOST + path;
    return axios
        .get(url, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const getCity = (stateId) => {
    let url = HOST + path + "/" + stateId;
    return axios
        .get(url, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const postCity = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}