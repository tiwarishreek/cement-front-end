import axios from "axios";

import { HOST, setHeader } from "../config";
const headers = setHeader();

const path = "type";

export const getAllType = () => {
    let url = HOST + path;
    return axios
    .get(url, setHeader)
    .then((response) => {
        return response;
    }).catch((error) => {
        throw new Error(error);
    });
}
export const getType = (materialId = '') => {
    let url = HOST + path + "/" + materialId;
    return axios
        .get(url, setHeader)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}


export const postType = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}