const cart = {
    cart: {
        id:'',
        qty:''
    },

    setCart(cart) {
        localStorage.setItem("cart", JSON.stringify(cart));
        this.cart = cart;
    },
    removeCart() {
        localStorage.removeItem("cart");
        this.cart = null;
    },
    getCart() {
        let cart = localStorage.getItem("cart");
        return JSON.parse(cart);
    },
};
export default cart;