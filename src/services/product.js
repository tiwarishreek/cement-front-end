import axios from "axios";

import { HOST, setHeader } from "../config";
const headers = setHeader();

const path = "product";



export const searchProduct = (input = {}) => {
    let url = HOST + path + "/search";
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const postProduct = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const updateProduct = (input = {}, id) => {
    let url = HOST + path + "/" + id;
    return axios
        .put(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const deleteProduct = (id) => {
    let url = HOST + path + "/" + id;
    return axios
        .delete(url, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}