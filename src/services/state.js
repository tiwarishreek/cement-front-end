import axios from "axios";

import { HOST,setHeader } from "../config";
const headers = setHeader();

const path = "state";

export const getState = () => {
    let url = HOST + path;
    return axios
        .get(url, setHeader)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const postState = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}