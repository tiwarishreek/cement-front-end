import axios from "axios";

import { HOST, setHeader } from "../config";
const headers = setHeader();

const path = "location";


export const getLocation = (cityId) => {
    let url = HOST + path + "/" + cityId;
    return axios
        .get(url,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const getAllLocation = () => {
    let url = HOST + path;
    return axios
        .get(url,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const postLocation = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}