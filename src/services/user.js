import axios from "axios";
import { HOST,setHeader } from "../config";
const headers = setHeader();

const path = "user";

export const postUser = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}

export const validateEmail = async (email) => {
    let url = HOST + "validate-email";
    return axios
        .post(url, {email}, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}



export const login = (input = {}) => {
    // let
    let url = HOST + "login";
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}

export const getAllUser = () => {
    let url = HOST + path;
    return axios
        .get(url,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const getUserById = (id) => {
    let url = HOST + path + "/"+ id ;
    return axios
        .get(url,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const updateUserById = (id, input={}) => {
    let url = HOST + path + "/"+ id ;
    return axios
        .put(url,input,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

