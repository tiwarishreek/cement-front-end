import axios from "axios";

import { HOST, setHeader } from "../config";
const headers = setHeader();

const path = "brand";

export const getBrand = (typeId) => {
    let url = HOST + path + "/" + typeId;
    return axios
        .get(url,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const getAllBrand = () => {
    let url = HOST + path;
    return axios
        .get(url,headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw new Error(error);
        });
}

export const postBrand = (input = {}) => {
    let url = HOST + path;
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}

export const searchBrand = (input = {}) => {
    let url = HOST + "brand/search";
    return axios
        .post(url, input, headers)
        .then((response) => {
            return response;
        }).catch((error) => {
            throw error;
        });
}

