import React, { Component } from 'react';
import auth from "./services/auth";

import { Route,Redirect } from 'react-router-dom'

export const PublicRoute = ({ component: Component, ...rest }) => (
    
    <Route {...rest} render={(props) => (
      auth.getAuth().isAuthenticated == "true"
        ? <Redirect to='/home' />
        : <Component {...props} />
    )} />
  )

